﻿using PapuaUtilities.GeneralClass;
//using PapuaUtilities.GeneralInfo;
//using PapuaUtilities.GeneralLog;
//using PapuaUtilities.GeneralMessage;
using PapuaWebForm.Helper.API;
using PapuaWebForm.Models.FirstLink.Instance.Transaction;
using PapuaWebForm.Models.FirstLink.Interface.Transaction;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PapuaWebForm.Models.FirstLink.Interface.Master;
using PapuaWebForm.Models.FirstLink.Instance.Master;
using PapuaWebForm.Models.FirstLink.Class.Transaction;
using System.Web.Services;
using PapuaUtilities.GeneralLog;

namespace PapuaWebForm.PEBPapuaUI.FirstLink_System.Training_Management.Transaction
{
    public partial class EventHandlingAttendanceDetail : System.Web.UI.Page
    {

        IEvent_dt IEvent = Inst_TrainingManagementDT.GetEvent_dt;
        IEvent_dtMember IEventMemb = Inst_TrainingManagementDT.GetEvent_dtMember;

        //IEvent_dtMemberAttd IEventMemberAtten = Inst_TrainingManagementDT.GetEvent_dtMemberAttd;
        //IMt_Program ImtProgram = Inst_TrainingManagementMT.GetMt_Program;

        private PapuaGeneralClass pgc = new PapuaGeneralClass();
        //private PapuaGeneralMessage pgm = new PapuaGeneralMessage();
        //private PapuaGeneralLog pgl = new PapuaGeneralLog();
        //private PapuaGeneralInfo pgi = new PapuaGeneralInfo();
        

        protected void Page_Load(object sender, EventArgs e)
        {
                if (!IsPostBack)
                {
                    GetTablePapuaUserOnline();
                    GetTablePapuaMenuDetail();
                    tampilForm();
                    //??????????????
                    hf_ModulName.Value = "EventHandlingAtten_" + hf_empno.Value.Trim() + "_" + hf_loginTime.Value.Trim();

                    if (!String.IsNullOrEmpty(hf_EventID.Value))
                    {
                        BindDetail();
                    }
                }
                if (!String.IsNullOrEmpty(hf_EventID.Value))
                {
                    string CookName="lastParti" + hf_EventID.Value.ToString();
                   
                    if (Request.Cookies[CookName] != null)
                    {
                        string lastpartiEmpNo = string.Empty;
                        string[] lastPartiInfo = Request.Cookies[CookName].Value.Split('|');
                        if (lastPartiInfo.Length > 1)
                        { 
                            lastpartiEmpNo = lastPartiInfo[1]; 
                            pgc.ShowEmplPhoto(lastpartiEmpNo,imgFoto); 
                        } 
                        lbStatus.Text = lastPartiInfo[0];
                        

                    }
                    BindDetail();
                    tbQR.Focus();
                    
                    
                }
                
        }
        protected void MyJS_ReLoad(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hf_EventID.Value))
            {
                BindDetail();
            }
        }
        private int harike(string start, string end)
        {

            DateTime startDate = Convert.ToDateTime(start);
            DateTime endDate = Convert.ToDateTime(end);
            DateTime today = DateTime.Now;

            int mystart = Convert.ToInt32(startDate.ToString("yyyyMMdd"));
            int myend = Convert.ToInt32(endDate.ToString("yyyyMMdd"));
            int dateToday = Convert.ToInt32(today.ToString("yyyyMMdd"));

            int DateRange = (myend - mystart) + 1;

            int myloop = 0;

            int dayIndex = 0;

            //datarange = 11
            //datetoday = 5

            for (int i = 0; i < DateRange; i++)
            {
                myloop = mystart + i;
                if (myloop == dateToday)
                {

                    dayIndex += i + 1;

                }

            }
            //string harike = "D : # " + dayIndex;
            return dayIndex;
        } 
 

        private void tampilForm()
        {
            DataTable dt = getDataTable();

            

            lbProgTitle.Text = dt.Rows[0]["progTitle"].ToString().Trim();
            lbProgGroupName.Text = dt.Rows[0]["ProgGrpName"].ToString().Trim();
            lbStartTime.Text = dt.Rows[0]["trainingStartTime"].ToString().Trim();
            lbEndTime.Text = dt.Rows[0]["trainingEndTime"].ToString().Trim();
            lblCount.Text = dt.Rows[0]["trainingDuration"].ToString().Trim();
            lbVenue.Text = dt.Rows[0]["venue"].ToString().Trim();
            lbProvider.Text = dt.Rows[0]["provName"].ToString().Trim();
            lbTrainer.Text = dt.Rows[0]["trainerName"].ToString().Trim();
            lbParticipantAtt.Text = dt.Rows[0]["participan_All"].ToString().Trim();


            lbDateAtten.Text = DateTime.Now.ToString("yyyy-MM-dd");

            string datestart = dt.Rows[0]["STARTDate"].ToString();
            string dateend = dt.Rows[0]["ENDDate"].ToString();
            int day = harike(datestart, dateend);
            lbharike.Text = "D : # " + day;

            btnApprove.Visible = false;
            btnReject.Visible = false;


           

        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
              
              
                
            }
        }

        private PapuaBLCommonParamWithApprovalExt getParamForDetail()
        {
            PapuaBLCommonParamWithApprovalExt pcpa = new PapuaBLCommonParamWithApprovalExt();
            pcpa.ManualQuery = " and a.eventID='" + hf_EventID.Value.ToString() + "' ";
            

            return pcpa;
        }

        private DataTable getDetailAttPivot(string eventID)
        {
            IEvent_dtMemberAttd IEventMemberAtten = Inst_TrainingManagementDT.GetEvent_dtMemberAttd;
            DataTable dt = new DataTable();
            dt = IEventMemberAtten.GetEvent_dtMemberAttdPivotByEventID(eventID);
            string[] hiddenCol = { "TotalRecord", "RowNumber", "eventID","memberID" };
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                for (int j = 0; j < hiddenCol.Length; j++)
                {
                    if (dt.Columns[i].ColumnName == hiddenCol[j])
                    {
                        dt.Columns.Remove(dt.Columns[i].ColumnName);
                    }
                }
            }
            return dt;
        }
        //private DataTable getDetail(PapuaBLCommonParamWithApprovalExt a)
        //{
        //    //PapuaBLCommonParamWithApprovalExt a = new PapuaBLCommonParamWithApprovalExt();
        //    DataTable dt = new DataTable();
        //    dt = IEventMemberAtten.GetEvent_dtMemberAttd(a);
        //    return dt;

        //}

        private void BindDetail()
        {
           // DataTable dt = getDetail(getParamForDetail());

            DataTable dt = getDetailAttPivot(hf_EventID.Value.ToString());
            //DataTable dt = getDataMember();
            pgc.setGVCaptionTotal(GVAttPivot, dt.Rows.Count.ToString());
            pgc.setGVCaptionTotal(gvtrainatten, dt.Rows.Count.ToString());

            if (dt.Rows.Count < 1)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
            GVAttPivot.DataSource = dt;
            GVAttPivot.DataBind();

         

        }

        private DataTable getDataTable()
        {
            PapuaBLCommonParamWithApprovalExt a = new PapuaBLCommonParamWithApprovalExt();
            DataTable dt = new DataTable();
            a.ParamFieldData1 = hf_EventID.Value;
            a.ParamField1 = "a.eventID"; 
            

            if (hf_tabIndex.Value == "0" || hf_tabIndex.Value == "")
            {
                 
                a.StepName = "ListINDUCTION";
                dt = IEvent.GetEvent_dt(a);
            }
            else if (hf_tabIndex.Value == "1" || hf_tabIndex.Value == "")
            {
                
                a.StepName = "ListSPECIFIC";
                dt = IEvent.GetEvent_dt(a);
            }
            else if (hf_tabIndex.Value == "2" || hf_tabIndex.Value == "")
            {
               
                a.StepName = "ListMANUAL";
                dt = IEvent.GetEvent_dt(a);
            }
            else if (hf_tabIndex.Value == "3" || hf_tabIndex.Value == "")
            {

                a.StepName = "ListMANUAL";
                a.ParamTab = "CompletedAtt";
                dt = IEvent.GetEvent_dt(a);
            }
            else
            {
             
                a.StepName = "ListMANUAL";
                a.ParamTab = "HistoryAtt";
                dt = IEvent.GetEvent_dt(a);
            }

            return dt;

        }


        private void GetTablePapuaUserOnline()
        {
            if (Request.QueryString["loginID"] != null)
            {
                string myLoginID = string.Empty;
                myLoginID = Request.QueryString["loginID"].ToString();
                OnlineID = myLoginID;

                PapuaUserOnline myLoginData = pgc.GetTablePapuaUserOnline(myLoginID);
                hf_loginTime.Value = myLoginData.LoginTime;
                hf_userlevel.Value = myLoginData.UserLevelID;
                hf_userID.Value = myLoginData.UserID;
                hf_empno.Value = myLoginData.EmployeeNo;
                hf_empname.Value = myLoginData.EmployeeName;
                hf_HuroDeptID.Value = myLoginData.DeptID;
                hf_HuroDivisiID.Value = myLoginData.DivisiID;
                hf_menuID.Value = myLoginData.MenuID;
            }
        }

        private string OnlineID
        {
            get { return hf_onlineID.Value; }
            set { hf_onlineID.Value = value; }
        }


        private void GetTablePapuaMenuDetail()
        {
            PapuaMenuDetail myMenuDetail = pgc.GetTablePapuaMenuDetail(OnlineID);
            hf_Add.Value = myMenuDetail.AddData;
            hf_Update.Value = myMenuDetail.UpdateData;
            hf_delete.Value = myMenuDetail.DeleteData;
            hf_ro.Value = myMenuDetail.ROData;
            hf_AllDataBool.Value = myMenuDetail.AllDataBool;
            hf_tabIndex.Value = myMenuDetail.ActiveTab;
            Hashtable ht = new Hashtable();

            if (myMenuDetail.QueryString is Hashtable)
            {
                ht = myMenuDetail.QueryString;
                if (ht.Contains("EventID"))
                    EventID = ht["EventID"].ToString().Trim();

            }
            else
            {
                ResponseRedirectPage();
            }
        }

        private void ResponseRedirectPage()
        {
            pgc.SetGeneralSearching(OnlineID, hf_AllDataBool.Value, hf_tabIndex.Value);
            Response.Redirect("EventHandlingAttendance.aspx?&LoginID=" + OnlineID);
        }
        private string EventID
        {
            get { return hf_EventID.Value; }
            set { hf_EventID.Value = value; }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            CloseForm();
        }
        private void CloseForm()
        {
            ResponseRedirectPage();
        }
        //private DataTable getDataMember()
        //{
        //    PapuaBLCommonParamWithApprovalExt a = new PapuaBLCommonParamWithApprovalExt();
        //    DataTable dt = new DataTable();
        //    a.ParamField1 = "a.eventID";
        //    a.Operrator1 = "=";
        //    a.ParamFieldData1 = hf_EventID.Value;
        //    dt = IEventMemb.GetEvent_dtMember(a);
        //    return dt;
        //}
        protected void ScanQR_Click(object sender, EventArgs e)
        {
           
        }

        //private bool saveUpdate(bool save, GridView gv)
        //{
        //    bool result = false;
        //    int i = 0;
        //    if (save)
        //    {
        //        i = IEventMemberAtten.Save(GetEvent_dtMemberAttd(true));
        //        if (i != 0)
        //            result = true;
        //    }
        //    else
        //    {
        //        result = IEventMemberAtten.Update(GetEvent_dtMemberAttd(false));
        //    }
        //    return result;
        //}

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            //insert emp_no
            //klik save, lalu tampil 
            //tap lagi


            //IEventMemberAtten


                //if (!string.IsNullOrEmpty(hf_EventID.Value))
                //{
                //    if (saveUpdate(true, gvMember))
                //    {
                //        PapuaGeneralMessage pgm = new PapuaGeneralMessage();
                //        BindGvMember();
                //    }
                //    else
                //    {
                //        PapuaGeneralMessage pgm = new PapuaGeneralMessage();
                //        pgm.MessageBoxAjaxs(updatePanel, pgm.msgSavedFail(), false);
                //    }
                //}

        }


        protected void btnReject_Click(object sender, EventArgs e)
        {
            BindDetail();
            string pesan = String.Empty;
            pesan = "You have been rejected this employee";
        }

        protected void tbQR_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hf_EventID.Value))
            {
                BindDetail();
            }
            
            

        }
        [WebMethod()]
        public static string CheckPartisipant(string EmpNo, string EventID, string AllowUnregist)
        {
            string myRes = string.Empty;

            string[] myEmpQRText = EmpNo.Split(';');
            string myEmpNo = string.Empty;
            if (myEmpQRText.Count() > 0) myEmpNo = myEmpQRText[0];

            if (!string.IsNullOrEmpty(myEmpNo))
            {
                EventHandlingAttendanceDetail myForm = new EventHandlingAttendanceDetail();
                PapuaBLCommonParamWithApprovalExt pcpa = new PapuaBLCommonParamWithApprovalExt();
                //myForm.getParamForDetail();
                Event_dtMember eventmem = new Event_dtMember();
                IEvent_dtMember IEventMember = Inst_TrainingManagementDT.GetEvent_dtMember;


                pcpa.ManualQuery += " and a.eventid='" + EventID + "' and a.employee_no='" + myEmpNo + "'";
                DataTable dt = IEventMember.GetEvent_dtMember(pcpa);
                if (dt.Rows.Count > 0)
                {
                   
                        //Save Attend....
                        Event_dtMemberAttd item = new Event_dtMemberAttd();
                        IEvent_dtMemberAttd IEventMemberAtten = Inst_TrainingManagementDT.GetEvent_dtMemberAttd;

                        item.EventID = EventID;
                        item.Employee_no = myEmpNo;
                        item.MemberID = dt.Rows[0]["memberID"].ToString();
                        item.AttDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

                        //GetData Attend orang yang TAP,,,-> EventID,EmployeeNo,Date
                        //Cek Jika Data sudah ada/Ditemukan, dan Date Attend sudah ada juga maka bilang, kamu udah
                        //else Jika DateAttend nya belum ada -- > maka Save Attend

                        string strparamManual = " and a.eventID='" + EventID + "' and a.Employee_no='" + myEmpNo + "' and convert(varchar(10),AttDate,20)='" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                        //----------------------------------------
                        DataTable dtAtt = IEventMemberAtten.GetEvent_dtMemberAttd(new PapuaBLCommonParamExt { ManualQuery = strparamManual });
                        if (dtAtt.Rows.Count > 0) //ini artinya ada
                        {

                            myRes = "[EXISTING]| You already join this training";
                        }
                        else
                        {
                            //item.MemberID = dt.Rows[0]["memberID"].ToString();
                            int mySav = IEventMemberAtten.SaveMulti(item);

                            if (mySav > 0)
                            {

                                //PapuaGeneralClass pgc = new PapuaGeneralClass();
                                //PapuaGeneralMessage pgm = new PapuaGeneralMessage();
                                // pgm.MessageBoxAjaxs(myForm.updatePanel, pgm.msgSaved(), false);
                                //DataTable mDt = myForm.getDetailAttPivot(EventID);                           
                                // myForm.GVAttPivot.DataSource = dt;
                                //  myForm.GVAttPivot.DataBind();

                                string myEmpName = dt.Rows[0]["Employee_Name"].ToString();
                              
                                myRes = "[SUCCESS]|" + "\n" + " Thank you for being present at this training" + "\n" + myEmpName + " .";
                            }
                            else
                            {
                                myRes = "[FAILED]| SAVE FAILED";
                            }
                        }
                    
                }
                else
                {

                    //Cek Jika Checkbox =checked , maka insert data ke DteventMember....
                    // get result insert , kal ada data, maka insert record Att...
                    // Jika insert record Att berhasil , maka myRes = "[SUCCESS]|
                    //else Cek Jika Checkbox !=checked  --> myRes = "[UNREGISTER]|

                    myRes = "[UNREGISTER]|  Sorry. You are not registered in this training.";

                  
                
                    if(AllowUnregist == "1" || AllowUnregist.ToLower() == "true"){

                        Event_dtMember evenmember = new Event_dtMember();
                        evenmember.EventID = EventID;
                        evenmember.Employee_no = myEmpNo;
                        
                        int save = IEventMember.Save(evenmember);

                        if (save > 0)
                        {
                            Event_dtMemberAttd item = new Event_dtMemberAttd();
                            IEvent_dtMemberAttd IEventMemberAtten = Inst_TrainingManagementDT.GetEvent_dtMemberAttd;

                            item.EventID = EventID;
                            item.Employee_no = myEmpNo;
                            item.AttDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                            item.MemberID = save.ToString();
                            int saveAtt = IEventMemberAtten.Save(item);

                            if (saveAtt > 0)
                            {
 
                                myRes = "[SUCCESS]|" + "\n" + " Thank you for being present at this training";
                            }
                            else
                            {
                                myRes = "[FAILED]| SAVE FAILED";
                            }

                        }
                    }
                }
            }
            return myRes;
        }
    }
}