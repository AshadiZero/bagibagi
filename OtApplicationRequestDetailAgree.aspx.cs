﻿using PapuaUtilities.GeneralClass;
using PapuaWebAPI.Models.UserAppr.Class.Master;
using PapuaWebForm.Models.GO.Instance.Master;
using PapuaWebForm.Models.GO.Interface.Master;
using PapuaWebForm.Models.Huro.Class.TransAttdOT;
using PapuaWebForm.Models.Huro.Instance.Master;
using PapuaWebForm.Models.Huro.Instance.TransAttdOT;
using PapuaWebForm.Models.Huro.Interface.Master;
using PapuaWebForm.Models.Huro.Interface.MasterAttdOT;
using PapuaWebForm.Models.Huro.Interface.TransAttdOT;
using PapuaWebForm.Models.UserAdmin.Instance.Transaction;
using PapuaWebForm.Models.UserAppr.Instance.Transaction;
using PapuaWebForm.Models.UserAppr.Interface.Transaction;
using PityHR.GenUtilities;
using PityHR.GenUtilitiesMail;
using PityHR.UserAdm.Instance;
using PityHR.UserAdm.Interface.Common_Approval;
using PityHR.UserAdm.Interface.UserAdm;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UI_AttendanceOT_Transaction_OtApplicationRequestDetailAgree : System.Web.UI.Page
{

    #region Variable
    private string classname = "PapuaWebForm.PEBPapuaUI.HURO_System.AttendanceOT.OtApplicationRequestDetailAgree";
  //  PapuaGeneralServices pgs = new PapuaGeneralServices();
    OverTime_dtHeader kls01;
    OverTime_dtDetail kls02;
    IOverTime_dtHeader mgr01;
    IOverTime_dtDetail mgr02;
    ICommon_Approval mgrAppr;
    GenUtilitiesMessage pgm = new GenUtilitiesMessage();
    PapuaWebForm.Models.Huro.Interface.Master.IWorkingShift shift;
    IPEBMTDept dept;
    IPEBMTSection section;
    IPEBMTLine line;
    IMtEmployee ime = PapuaInstanceMasterHR.GetInstanceMtEmployee;
    GenUtilitiesMailTemplate pmt = new GenUtilitiesMailTemplate();
    GenUtilitiesSendMail pgsm = new GenUtilitiesSendMail();
    GenUtilitiesHelper guh = new GenUtilitiesHelper();
    PapuaGeneralClass pgc = new PapuaGeneralClass();
    IAPOUserDAO Inst_apouser = PapuaInstancePapuaAdminDT.GetInstanceAPOUser;
    ICommon_Approval ica = InstanceCommonApproval.GetCommon_Approval;
    ICommonFunction ifun = PapuaInstanceCommonFunction.GetInstanceCommonFunction;

    private string tbl01 = "New";
    private string tbl02 = "OnProgress";
    private string tbl03 = "Approval";
    private string tbl04 = "Reject";
    private string tbl05 = "Complete";
    private string tbl06 = "HISTORY";
    private string[] ExcelField = new string[] { "OTDetailsID", "OTdtID", "IsProposed", "ProposeId", "isAgree", "IsOut", "mealDuration", "actualTimeFrom", "actualTimeTo" };
    private string tblOTPropose = "TblOTPropose";

    IAttdForceRelease inst_attdForce = InstAttendanceOTDT.GetInstanceAttdForceReleased;
    IOverTime_dtDetail inst_detail = InstAttendanceOTDT.GetInstanceOverTime_dtDetail;
    IOverTime_dtHeader inst_header = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;

    string blank = "blank";
    string taskname;
    private string applicationName = "GO-eOT";
    private static string subjectApp = "Application Request OT ";
    private static string appType = "Request";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

        SetRegisteredControlToUpdatePanelTrigger();

        if (!IsPostBack)
        {
            //HF_OTDTID.Value = "";

            getSession();
            this.pgc.setUserAuth(hf_Add, hf_Update, hf_delete, hf_ro, hf_AllDataBool, hf_deptID.Value.Trim(), hf_deptID.Value.Trim(), this.Page);
            WebHelper.getTitlebyMenu(lblTitle);
           
            SetClearData();
            GetTransferData();
            hfAppStep.Value = "0";
            //GetTablePapuaUserOnline();
            //GetTablePapuaMenuDetail();
            getReportPeriode();

            SetShiftCode(ddlShiftCode);

            BindForm();

            //if (GetSession())
            //{

            //    getReportPeriode();
            //    SetClearData();
            //    SetShiftCode(ddlShiftCode);

            //    BindForm();
            //}
            //else
            //{
            //    Response.Write("Applicaton not available !,Please call ISD System Supoort for further information !");
            //}

            Session.Remove("OtID" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
            Session.Remove("sesTab" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
            Session.Remove("OtTypeForm" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
     

        }
    }

    private void GetTransferData()
    {
        //Page originalPage = (Page)Context.Handler;
        //HiddenField hf = (HiddenField)originalPage.FindControl("hfTransfer");
        //hfTransfer.Value = hf.Value;
        //OtID = textCut(hfTransfer.Value, 1);
        //ModeView = textCut(hfTransfer.Value, 2);
        //OtTypeForm = textCut(hfTransfer.Value, 3);
        //hf_menuID.Value = textCut(hfTransfer.Value, 4);
        
        OtID = (string)Session["OtID" + hf_empno.Value.Trim() + hf_deptID.Value.Trim()];
        OtTypeForm = (string)Session["OtTypeForm" + hf_empno.Value.Trim() + hf_deptID.Value.Trim()];
        ModeView = (string)Session["sesTab" + hf_empno.Value.Trim() + hf_deptID.Value.Trim()];
        HF_OTDTID.Value = OtID;
     



        if (HF_OTDTID.Value != null)
        {
            //HFOTDTID.Value = Session["OTDTID"].ToString();
            //if (Session["OTTypeForm"] != null)
            //    HFOTTypeForm.Value = Session["OTTypeForm"].ToString();

            if (HFOTTypeForm.Value.ToLower() == "Abnormal".ToLower())
            {
              //  hf_taskID.Value = "192";
                HFOTTypeForm.Value = "0";
                lblOTForm.Text = HFOTTypeForm.Value;
                panelForm.GroupingText = "OT Abnormal Form";
                ////lblHeader.InnerText = "OT Abnormal List ";
                lblTitle.Text = "OT Abnormal List";
                visibleToAbnormal();
            }
            else
            {
               // hf_taskID.Value = "191";
                HFOTTypeForm.Value = "1";
                lblOTForm.Text = HFOTTypeForm.Value;
                panelForm.GroupingText = "OT Form";
                //lblHeader.InnerText = "OT Normal List"; 
                lblTitle.Text = "OT Normal List";
                //  GeneralTitleHeader.setCustomTitle("OT Normal List");
            }



        }
        else
        {
            this.ResponseRedirectPage();
        }


    }

    private string textCut(string text, int nilai)
    {
        int i = nilai - 1;
        string[] txt = null;

        try
        {
            txt = text.Split(new Char[] { '|' });
            return txt[i];
        }
        catch
        {
            return txt[0];
        }
    }

    private void getReportPeriode()
    {
        DataTable dtdate = new DataTable();
        dtdate = inst_attdForce.GetDateFY();
        lblDateFrom.Text = dtdate.Rows[0]["date_from"].ToString();
        lblDateTo.Text = dtdate.Rows[0]["date_to"].ToString();
    }

    #region Session
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }


    private void getSession()
    {
        if (Session["userLogID"] != null)
        {
            hf_onlineID.Value = Session["userLogID"].ToString();
            DataTable dt = WebHelper.GetDataUserInfo(hf_onlineID.Value.Trim());
            DataTable dtTemp = WebHelper.SessionUserAuthGetter();
            if (Session["menu_id"] != null)
            {
                string menuid = Session["menu_id"].ToString();
                hf_menuID.Value = guh.Decrpt2Way(menuid, WebHelper.EncriptKeyOnlineID);
                //hf_task_id.Value = guh.getTaskID(guh.Decrpt2Way(menuid, WebHelper.EncriptKeyOnlineID));

                //DataTable dtTemp = gul.GetMenuAuth(menuid, "1", "1");                    
            }
            setSessionToField(dt);
        }
        else
        {
            WebHelper.IsLogin();

            if (WebHelper.SessionIsExist(WebHelper.SessionNameUserInfo))
            {
                Session.Remove(WebHelper.SessionNameUserInfo);
                string a = Session["IsLogin"].ToString();
                hf_onlineID.Value = a;
                DataTable dt = WebHelper.GetDataUserInfo(hf_onlineID.Value.Trim());
                if (Session["menu_id"] != null)
                {
                    string menuid = Session["menu_id"].ToString();
                    hf_menuID.Value = guh.Decrpt2Way(menuid, WebHelper.EncriptKeyOnlineID);
                    //  hf_task_id.Value = getTaskID(guh.Decrpt2Way(menuid, WebHelper.EncriptKeyOnlineID));
                    //DataTable dtTemp = gul.GetMenuAuth(menuid, "1", "1");                    
                }
                bool c = false;
                setSessionToField(dt);
            }
        }
    }
    private void setSessionToField(DataTable dtTemp)
    {
        for (int i = 0; i < dtTemp.Rows.Count; i++)
        {
            hf_empno.Value = dtTemp.Rows[i]["employee_no"].ToString();
            hf_empname.Value = dtTemp.Rows[i]["employee_name"].ToString();
            hf_userID.Value = dtTemp.Rows[i]["user_id"].ToString();
            hf_deptID.Value = dtTemp.Rows[i]["dept_id"].ToString();
            hf_dept.Value = dtTemp.Rows[i]["employ_Deptname"].ToString();
            hf_HuroDeptID.Value = dtTemp.Rows[i]["employ_Deptid"].ToString();
            hf_HuroDivisiID.Value = dtTemp.Rows[i]["divID"].ToString();
            hf_divisi.Value = dtTemp.Rows[i]["divisiName"].ToString();
            hf_divisiID.Value = dtTemp.Rows[i]["divisi_id"].ToString();
            hf_userlevel.Value = dtTemp.Rows[i]["user_level_id"].ToString();
            hf_loginTime.Value = DateTime.Now.ToLongTimeString() + new Random().Next(1000).ToString() + new Random().NextDouble().ToString(); //myLoginData.LoginTime.Trim();
        }
    }
    private void setDataSession(DataTable dtTemp, string sessionName)
    {
        WebHelper.SessionSetData(sessionName, dtTemp);
    }

    #endregion Session

    //private void GetTablePapuaUserOnline()
    //{


    //    if (Request.QueryString["loginID"] != null)
    //    {
    //        string myLoginID = string.Empty;
    //        myLoginID = Request.QueryString["loginID"].ToString();
    //        OnlineID = myLoginID;

    //        PapuaUserOnline myLoginData = pgc.GetTablePapuaUserOnline(myLoginID);
    //        hf_loginTime.Value = myLoginData.LoginTime;
    //        hf_userlevel.Value = myLoginData.UserLevelID;
    //        hf_userID.Value = myLoginData.UserID;
    //        hf_deptID.Value = myLoginData.AppDeptID;
    //        hf_dept.Value = myLoginData.DeptName;
    //        hf_empno.Value = myLoginData.EmployeeNo;
    //        hf_empname.Value = myLoginData.EmployeeName;
    //        hf_HuroDeptID.Value = myLoginData.DeptID;
    //        hf_HuroDivisiID.Value = myLoginData.DivisiID;
    //        hf_menuID.Value = myLoginData.MenuID;
    //        hf_divisi.Value = myLoginData.DivisiName;
    //        hf_divisiID.Value = myLoginData.DivisiID;
    //        hf_ipaddress.Value = myLoginData.IPAddress;
    //        hf_loginTime.Value = myLoginData.LoginTime;

    //        /*Select IDOnline,SessionID,IPAddress,MenuID,MenuName,MenuUrl,UserLevelID,UserLevelName,AppID,AppName,AppDeptID,AppDeptName,UserID,EmployeeName,EmployeeNo,AccountName,Office,FullName,
    //         * Email,DivisiID,DivisiName,DeptID,DeptName,SectionID,SectionName,LineID,LineName,Posisi,LevelEmployee,Status,LoginTime*/

    //    }


    //}
    private string OtID
    {
        get { return HF_OTDTID.Value; }
        set
        {
            HF_OTDTID.Value = value;
        }
    }
    private string TabOtApplyMain
    {
        get { return hf_tabOtApplyMain.Value; }
        set
        {
            hf_tabOtApplyMain.Value = value;
        }
    }

    private string OtTypeForm
    {
        get { return HFOTTypeForm.Value; }
        set
        {
            HFOTTypeForm.Value = value;
        }
    }
    private string ModeView
    {
        get { return hf_modeView.Value; }
        set
        {
            hf_modeView.Value = value;
        }
    }
    //private void GetTablePapuaMenuDetail()
    //{
    //    PapuaMenuDetail myMenuDetail = pgc.GetTablePapuaMenuDetail(OnlineID);
    //    hf_Add.Value = myMenuDetail.AddData;
    //    hf_Update.Value = myMenuDetail.UpdateData;
    //    hf_delete.Value = myMenuDetail.DeleteData;
    //    hf_ro.Value = myMenuDetail.ROData;
    //    hf_AllDataBool.Value = myMenuDetail.AllDataBool;


    //    Hashtable ht = new Hashtable();
    //    if (myMenuDetail.QueryString is Hashtable)
    //    {
    //        ht = myMenuDetail.QueryString;
    //        if (ht.Contains("ParamBEI"))
    //            OtID = ht["ParamBEI"].ToString();
    //        if (ht.Contains("paramst"))
    //            ModeView = ht["paramst"].ToString();
    //        if (ht.Contains("paramBEI2"))
    //            OtTypeForm = ht["paramBEI2"].ToString();
    //        if (ht.Contains("MenuIDMain"))
    //            hf_menuID.Value = ht["MenuIDMain"].ToString().Trim();


    //        if (HF_OTDTID.Value != null)
    //        {
    //            //HFOTDTID.Value = Session["OTDTID"].ToString();
    //            //if (Session["OTTypeForm"] != null)
    //            //    HFOTTypeForm.Value = Session["OTTypeForm"].ToString();

    //            if (HFOTTypeForm.Value.ToLower() == "Abnormal".ToLower())
    //            {
    //                ////hf_taskID.Value = "192";
    //                HFOTTypeForm.Value = "0";
    //                lblOTForm.Text = HFOTTypeForm.Value;
    //                panelForm.GroupingText = "OT Abnormal Form";
    //                ////lblHeader.InnerText = "OT Abnormal List ";
    //                lblTitle.Text ="OT Abnormal List";
    //                visibleToAbnormal();
    //            }
    //            else
    //            {
    //                //hf_taskID.Value = "191";
    //                HFOTTypeForm.Value = "1";
    //                lblOTForm.Text = HFOTTypeForm.Value;
    //                panelForm.GroupingText = "OT Form";
    //                //lblHeader.InnerText = "OT Normal List"; 
    //                lblTitle.Text = "OT Normal List";
    //              //  GeneralTitleHeader.setCustomTitle("OT Normal List");
    //            }



    //        }
    //        else
    //        {
    //            this.ResponseRedirectPage();
    //        }

    //    }
    //}
    private void ResponseRedirectPage()
    {
        //  pgc.SetGeneralSearching(OnlineID, hf_AllDataBool.Value, TabOtApplyMain);

        //string ParamQuery = string.Empty;
        //ParamQuery = "&MenuIDMain=" + hf_menuID.Value;
        // pgc.SetQueryString(OnlineID, ParamQuery);
        SetSession();
        Response.Redirect("OtApplicationRequest.aspx");
    }
    private string OnlineID
    {
        get { return hf_onlineID.Value; }
        set
        {
            hf_onlineID.Value = value;
        }
    }
    //private bool GetSession()
    //{
    //    bool bola = false;

    //    hf_modeView.Value = Request.QueryString["paramst"].ToString();
    //    hf_modeView.Value = Encoding.Unicode.GetString(Convert.FromBase64String(hf_modeView.Value));



    //    if (Session["LoginTime"] != null)
    //        hf_loginTime.Value = Session["LoginTime"].ToString();
    //    if (Session["user_level_id"] != null)
    //        hf_userlevel.Value = Session["user_level_id"].ToString();
    //    if (Session["user_id"] != null)
    //        hf_userID.Value = Session["user_id"].ToString();
    //    if (Session["deptid"] != null)
    //        hf_deptID.Value = Session["deptid"].ToString();
    //    if (Session["employee_no"] != null)
    //        hf_empno.Value = Session["employee_no"].ToString();
    //    if (Session["employee_name"] != null)
    //        hf_empname.Value = Session["employee_name"].ToString();
    //    if (Session["HURODeptID"] != null)
    //        hf_HuroDeptID.Value = Session["HURODeptID"].ToString();
    //    if (Session["HUROdivisiID"] != null)
    //        hf_HuroDivisiID.Value = Session["HUROdivisiID"].ToString();
    //    if (Session["menu_ID"] != null)
    //        hf_menuID.Value = Session["menu_ID"].ToString();
    //    if (Session["dept_huro"] != null)
    //        hf_dept.Value = Session["dept_huro"].ToString();
    //    if (Session["divisi"] != null)
    //        hf_divisi.Value = Session["divisi"].ToString();
    //    if (Session["ipaddress"] != null)
    //        hf_ipaddress.Value = Session["ipaddress"].ToString();

    //    //HF_OTDTID.Value = Request.QueryString["ParamBEI"].ToString();
    //    //HF_OTDTID.Value = Encoding.Unicode.GetString(Convert.FromBase64String(HF_OTDTID.Value));

    //    //if (Session["OTDTID"] != null)

    //    if (HF_OTDTID.Value != null)
    //    {
    //        //HFOTDTID.Value = Session["OTDTID"].ToString();
    //        //if (Session["OTTypeForm"] != null)
    //        //    HFOTTypeForm.Value = Session["OTTypeForm"].ToString();

    //        if (HFOTTypeForm.Value =="0")
    //        {
    //            //hf_taskID.Value = "192";
    //            HFOTTypeForm.Value = "0";
    //            lblOTForm.Text = HFOTTypeForm.Value;
    //            panelForm.GroupingText = "OT Abnormal Form";
    //            //lblHeader.InnerText = "OT Abnormal List ";
    //            GeneralTitleHeader.setCustomTitle("OT Abnormal List");
    //            visibleToAbnormal();
    //        }
    //        else
    //        {
    //            //hf_taskID.Value = "191";
    //            HFOTTypeForm.Value = "1";
    //            lblOTForm.Text = HFOTTypeForm.Value;
    //            panelForm.GroupingText = "OT Form";
    //            //lblHeader.InnerText = "OT Normal List";
    //            GeneralTitleHeader.setCustomTitle("OT Normal List");
    //        }

    //        bola = true;
    //    }
    //    return bola;
    //}



    private void SetSession()
    {
        //Session["dept_huro"] = hf_dept.Value; 
        Session["divisi"] = hf_divisi.Value;

        Session["OTDTID"] = HF_OTDTID.Value;

        if (Session["LoginTime"] != null)
            Session.Remove("LoginTime");
        Session["LoginTime"] = hf_loginTime.Value.Trim();
        if (Session["user_level_id"] != null)
            Session.Remove("user_level_id");
        Session["user_level_id"] = hf_userlevel.Value.Trim();
        if (Session["user_id"] != null)
            Session.Remove("user_id");
        Session["user_id"] = hf_userID.Value.Trim();
        if (Session["deptid"] != null)
            Session.Remove("deptid");
        Session["deptid"] = hf_deptID.Value.Trim();
        if (Session["deptName"] != null)
            Session.Remove("deptName");
        if (Session["employee_no"] != null)
            Session.Remove("employee_no");
        Session["employee_no"] = hf_empno.Value.Trim();
        if (Session["employee_name"] != null)
            Session.Remove("employee_name");
        Session["employee_name"] = hf_empname.Value.Trim();
        if (Session["menu_ID"] != null)
            Session.Remove("menu_ID");
        Session["menu_ID"] = guh.Encrpt2Way(hf_menuID.Value.Trim(), WebHelper.EncriptKeyOnlineID);

        if (Session["HURODeptID"] != null)
            Session.Remove("HURODeptID");
        Session["HURODeptID"] = hf_HuroDeptID.Value.Trim();
        if (Session["HUROdivisiID"] != null)
            Session.Remove("HUROdivisiID");
        Session["HUROdivisiID"] = hf_HuroDivisiID.Value.Trim();
        //Session["OTTypeForm"] = HFOTTypeForm.Value;
        //TypeForm : 0 Normal : 1 AbNormal    
        //private enum OTTypeForm { Normal = 0, Abnormal =1};
    }

    #region Control
    private void SetShiftCode(DropDownList dd)
    {
        shift = PapuaInstanceWorkingShift.GetInstanceWorkingShift;
        pgc.setControlList(shift.GetDistinctShiftCode(), "shiftcode", dd);
        dd.Items.Insert(0, "");
        dd.SelectedIndex = dd.Items.IndexOf(dd.Items.FindByText(""));
    }
    private void SetDept(string divid, DropDownList dd)
    {
        dept = PapuaInstancePebChart.GetInstancePEBMTDept;
        pgc.setControlList(dept.GetDeptByPapuaOrgChart(hf_menuID.Value, hf_userID.Value, lbldivID.Text), "deptid", "dept", dd);
        dd.Items.Insert(0, "");
        dd.SelectedIndex = dd.Items.IndexOf(dd.Items.FindByText(""));
    }
    private void SetSection(string deptid, DropDownList dd)
    {
        section = PapuaInstancePebChart.GetInstancePEBMTSection;
        pgc.setControlList(section.GetSectionByParam("a.deptid", deptid, "="), "sectionid", "sections", dd);
        dd.Items.Insert(0, "");
        dd.SelectedIndex = dd.Items.IndexOf(dd.Items.FindByText(""));

    }
    private void SetLIne(string sectionid, DropDownList dd)
    {
        line = PapuaInstancePebChart.GetInstancePEBMTLine;
        pgc.setControlList(line.GetLineByParam("a.sectionid", sectionid, "="), "lineid", "line", dd);
        dd.Items.Insert(0, "");
        dd.SelectedIndex = dd.Items.IndexOf(dd.Items.FindByText(""));
    }


    private void SetRegisteredControlToUpdatePanelTrigger()
    {
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(btnexcell);

    }
   
    #endregion

    #region GridViewPage
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private void setSorting(GridView gv, object sender, GridViewSortEventArgs e, DataTable dtTemp)
    {
        GridViewSortExpression = e.SortExpression;
        this.pgc.GVSortExpression = GridViewSortExpression;
        this.pgc.GVSortDirection = GridViewSortDirection;
        int pageIndex = gv.PageIndex;
        gv.DataSource = this.pgc.SortDataTable(dtTemp, false);
        GridViewSortDirection = this.pgc.GVSortDirection;
        gv.DataBind();
        gv.PageIndex = pageIndex;
    }
    private void setPaging(GridView gv, object sender, GridViewPageEventArgs e, DataTable dtTemp)
    {
        gv.SelectedIndex = -1;
        this.pgc.GVSortExpression = GridViewSortExpression;
        this.pgc.GVSortDirection = GridViewSortDirection;
        gv.DataSource = this.pgc.SortDataTable(dtTemp, true);
        gv.PageIndex = e.NewPageIndex;
        //gv.PageIndex=2;
        gv.DataBind();



        string pageIndex = gv.PageIndex.ToString();
    }
    private void setRowsNum(GridView gv, string numrows, DataTable dtTemp)
    {
        int data = 0;
        int data1 = 0;
        data = this.pgc.safeInt(numrows);
        data1 = dtTemp.Rows.Count;
        if (data > 0)
        {
            if (data1 > 0)
            {
                if (data1 < data)
                    data = data1;
                gv.DataSource = pgc.SortDataTable(dtTemp, true);
                gv.PageSize = data;
                gv.DataBind();
            }
        }
    }
    private void setPageIndex(GridView gv, string indeks, DataTable dtTemp)
    {
        int indek = 0;
        int data1 = 0;
        int data2 = 0;
        indek = this.pgc.safeInt(indeks);
        if (indek <= 0)
            indek = 1;
        indek = indek - 1;
        data1 = dtTemp.Rows.Count;
        if (data1 > 0)
        {
            data2 = gv.PageCount;
            if (indek < 0 || indek > data2)
                indek = data2;
            gv.DataSource = pgc.SortDataTable(dtTemp, true);
            gv.PageIndex = indek;
            gv.DataBind();
        }
    }
    #endregion


    #region GridViewEvent
    protected void tabPapua_ActiveTabChanged(object sender, EventArgs e)
    {
        //string tableName = this.GridViewTablename();
        //BindDataGV(GridViewGetName(), GetDataGrid(false, tableName, SetParameter(tableName)));
    }
    protected void GV_Sorting(object sender, GridViewSortEventArgs e)
    {
        //string tableName = this.GridViewTablename();
        //this.setSorting(((GridView)sender), sender, e, GetDataGrid(false, tableName, SetParameter(tableName)));
    }
    protected void GV_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        string GVID = ((GridView)sender).ID.ToString();
        DataTable dtTemp = new DataTable();
        mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
        string section = string.Empty;
        string line = string.Empty;
        string PlanTimeFrom = string.Empty;
        string PlanTimeTo = string.Empty;
        string shift = string.Empty;
        string date = string.Empty;

        DataTable dthed = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", HF_OTDTID.Value.ToString(), "", "=", "", "", "", "", ""));
        if (dthed.Rows.Count > 0)
        {
            lbldivID.Text = dthed.Rows[0]["divisiID"].ToString();
            hf_deptID.Value = dthed.Rows[0]["deptID"].ToString();
        }

        if (GVID == "GV01")
        {
            if (pgc.setAccessSave(ddlSection))
            {
                section = pgc.getDropdownValue(ddlSection);
                if (pgc.setAccessSave(ddlLine))
                {
                    line = pgc.getDropdownValue(ddlLine);
                    if (pgc.setAccessSave(ddlShiftCode))
                    {
                        shift = pgc.getDropdownValue(ddlShiftCode);
                        if (pgc.setAccessSave(txtDate))
                        {
                            date = txtDate.Text;
                            if (pgc.setAccessSave(txtEndTime, txtstarttime))
                            {
                                PlanTimeFrom = date + " " + txtstarttime.Text.Replace(".", ":");
                                PlanTimeTo = date + " " + txtEndTime.Text.Replace(".", ":");
                            }

                        }
                    }
                }
            }

            dtTemp = mgr01.GetOverTimeEmployee(lbldivID.Text, hf_HuroDeptID.Value, section, line, PlanTimeFrom, PlanTimeTo, shift, HFOTTypeForm.Value);

            setPaging(GV01, sender, e, dtTemp);
        }
        

    }
 

    protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string GVID = ((GridView)sender).ID.ToString();


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblNumber = (Label)e.Row.FindControl("lblNo");
            lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (((GridView)sender).PageIndex * ((GridView)sender).PageSize));


            if (GVID == "GV01")
            {
                if (!string.IsNullOrEmpty(HF_OTDTID.Value))
                {
                    sembunyiHF(e);
                }
                else
                {
                    sembunyiHF(e, true);
                }

                TextBox adjus = (TextBox)e.Row.FindControl("txtAdjDuration");
                if (adjus.Text.Trim() != "")
                {
                    e.Row.BackColor = System.Drawing.Color.AntiqueWhite;
                }

                Label lblcaltypeIDgv = (Label)e.Row.FindControl("lblcaltypeID");
                Label lblEmpNo = (Label)e.Row.FindControl("lblEmpNo");
                if (lblcaltypeIDgv.Text != "1")
                {
                    lblEmpNo.Font.Bold = true;
                    if (lblcaltypeIDgv.Text == "2")
                    {
                        pgc.setControlVisible(true, lblNote, lblg1);
                        lblEmpNo.ForeColor = System.Drawing.Color.FromArgb(0, 179, 60);
                    }
                    else if (lblcaltypeIDgv.Text == "3")
                    {
                        pgc.setControlVisible(true, lblNote, lblg1, lblg2);
                        lblEmpNo.ForeColor = System.Drawing.Color.FromArgb(255, 128, 128);
                    }
                    else if (lblcaltypeIDgv.Text == "4")
                    {
                        pgc.setControlVisible(true, lblNote, lblg1, lblg2, lblg3);
                        lblEmpNo.ForeColor = System.Drawing.Color.FromArgb(179, 102, 255);
                    }


                    //lblNote.Text = "nb :" + lblg1.Text.Trim() +" "+ lblg2.Text.Trim() +" "+ lblg3.Text.Trim();

                }

            }

            else if (e.Row.RowType == DataControlRowType.Header)
            {

            }


        }
    }
    private void SetDataSession(DataTable dtTemp, string sessioName)
    {
        if (Session[sessioName] != null)
        {
            Session.Remove(sessioName);
        }
        Session[sessioName] = dtTemp;
    }

    private void SetStringSession(string strTemp, string sessioName)
    {
        if (Session[sessioName] != null)
        {
            Session.Remove(sessioName);
        }
        Session[sessioName] = strTemp;
    }


    protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Control source = e.CommandSource as Control;
        GridViewRow row = source.NamingContainer as GridViewRow;

        if (e.CommandName == "liatPropose")
        {

            HiddenField hf_purposeID = (HiddenField)GV01.Rows[row.RowIndex].FindControl("hf_purposeID");
            string BEI = hf_purposeID.Value.ToString();
            string strstatus = tblOTPropose;
            string otdtid = HF_OTDTID.Value;
            string modelview = "Application";
            string type = HFOTTypeForm.Value;

            string paramquerystring = "&ParamBEI=" + BEI + "&paramst=" + strstatus + "&otdtid=" + otdtid + "&modelview=" + modelview + "&type=" + type;
            //pgc.SetQueryString(OnlineID, paramquerystring);

            string ParamQuery = string.Empty;
            ParamQuery = paramquerystring + "&MenuIDMain=" + hf_menuID.Value;
            //  pgc.SetQueryString(OnlineID, ParamQuery);
            SetSession();
            SetStringSession(BEI, "BEIOT" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
            SetStringSession(modelview, "sesTab" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
            SetStringSession(otdtid, "OTDTID" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
            //SetStringSession(type, "sesTab" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());
            SetStringSession(hf_purposeID.Value.ToString(), "purposeID" + hf_empno.Value.Trim() + hf_deptID.Value.Trim());

            Response.Redirect("OtProposeDetail.aspx");
        }

        else if (e.CommandName == "View")
        {
            DataTable dtAtt = new DataTable();
            IPapuaAttdMonitoring monitoring = InstAttendanceOTDT.GetInstanceAttdMonitoring;
            Label lblEmpNo = (Label)GV01.Rows[row.RowIndex].FindControl("lblEmpNo");

            if (!string.IsNullOrEmpty(lblEmpNo.Text))
            {
                //  panelAtt.Visible = true;
                // pgc.setControlVisible(true, gvAtt);
                dtAtt = monitoring.AttdEmpList(1, "", "", "", "", "", "", "", lblEmpNo.Text.Trim(), txtDate.Text, txtDate.Text, 0, 0, "", "", 1);
                if (dtAtt.Rows.Count == 0)
                    dtAtt = monitoring.AttdEmpList(1, "", "", "", "", "", "", "", lblEmpNo.Text.Trim(), txtDate.Text, txtDate.Text, 0, 0, "", "", 99);

                //  BindDataGV(gvAtt, dtAtt);

            }
            else
            {
                //pgc.setControlVisible(true, gvAtt);
            }
        }
         
        else if (e.CommandName.ToLower() == "agreeot")
        {
            IOverTime_dtDetail myOT = InstAttendanceOTDT.GetInstanceOverTime_dtDetail;
            List<PapuaBLCommon> myLi = new List<PapuaBLCommon>();
            PapuaBLCommon myBL = new PapuaBLCommon();

            myBL.FieldName = "isAgree";
            myBL.Value = "1";
            myLi.Add(myBL);

            myBL = new PapuaBLCommon();
            myBL.FieldName = "AgreeBy";                  //Need Add FIeld
            myBL.Value = hf_empno.Value.ToString();
            myLi.Add(myBL);

            myBL = new PapuaBLCommon();
            myBL.FieldName = "AgreeDate";                 //Need Add field
            myBL.Value = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            myLi.Add(myBL);


            Label lblDetailsID = (Label)GV01.Rows[row.RowIndex].FindControl("lblDetailsID");
            Label lblMemberName = (Label)GV01.Rows[row.RowIndex].FindControl("lblEmpName");


            bool OtAgree = myOT.SaveUpdateModifiedOverTime_dtDetail(false, myLi, "OTDetailsID", lblDetailsID.Text);
            if (OtAgree)
            {
                WebHelper.MessageBoxAjaxs(UPUtama, "Success .  \n OT Agreement "+ lblMemberName .Text+ " has been submited. [AGREE]", false);
                

                PapuaBLCommonParamExt pcpe = new PapuaBLCommonParamExt();
                pcpe.ParamField1 = "A.OTdtID";
                pcpe.ParamFieldData1 = HF_OTDTID.Value.ToString();
                pcpe.Operrator1 = "=";
                DataTable dtdet = inst_detail.GetOverTime_dtDetail(pcpe);
                BindDataGV(GV01, dtdet);
               
            }


        }
        else if (e.CommandName.ToLower() == "dissagreeot")
        {
            IOverTime_dtDetail myOT = InstAttendanceOTDT.GetInstanceOverTime_dtDetail;
            List<PapuaBLCommon> myLi = new List<PapuaBLCommon>();
            PapuaBLCommon myBL = new PapuaBLCommon();

            myBL.FieldName = "isAgree";
            myBL.Value = "0";
            myLi.Add(myBL);

            myBL = new PapuaBLCommon();
            myBL.FieldName = "AgreeBy";                  //Need Add FIeld
            myBL.Value = hf_empno.Value.ToString();
            myLi.Add(myBL);

            myBL = new PapuaBLCommon();
            myBL.FieldName = "AgreeDate";                 //Need Add field
            myBL.Value = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            myLi.Add(myBL);


            Label lblDetailsID = (Label)GV01.Rows[row.RowIndex].FindControl("lblDetailsID");
            Label lblMemberName = (Label)GV01.Rows[row.RowIndex].FindControl("lblEmpName");


            bool OtAgree = myOT.SaveUpdateModifiedOverTime_dtDetail(false, myLi, "OTDetailsID", lblDetailsID.Text);
            if (OtAgree)
            {
                WebHelper.MessageBoxAjaxs(UPUtama, "Success .  \n OT Agreement " + lblMemberName.Text + " has been submited.[DISSAGREE].", false);


                PapuaBLCommonParamExt pcpe = new PapuaBLCommonParamExt();
                pcpe.ParamField1 = "A.OTdtID";
                pcpe.ParamFieldData1 = HF_OTDTID.Value.ToString();
                pcpe.Operrator1 = "=";
                DataTable dtdet = inst_detail.GetOverTime_dtDetail(pcpe);
                BindDataGV(GV01, dtdet);

            }
        }

    }
    #endregion

    private string getDeptUser()
    {
        IPEBMTDept IPDept = PapuaInstancePebChart.GetInstancePEBMTDept;
        DataTable dttemp = IPDept.GetDeptByPapuaOrgChartWithoutDiv(hf_menuID.Value, hf_userID.Value, hf_AllDataBool.Value);
        string filter = string.Empty;
        string query = string.Empty;

        for (int i = 0; i < dttemp.Rows.Count; i++)
        {
            string aaa = dttemp.Rows[i]["deptid"].ToString().Trim();
            filter = aaa;
            if (i < (dttemp.Rows.Count - 1))
                filter = filter + ", ";
            query += filter;
        }
        return query;
    }

    #region setHeader
    private void SetOTForm()
    {
        if (!string.IsNullOrEmpty(HF_OTDTID.Value))
        {
            //Set Get From DB
            //di BindForm

        }
        else
        {
            lblrefID.Text = hf_menuID.Value + "-" + HF_OTDTID.Value + " ";
            lblDivision.Text = hf_divisi.Value;
            hf_divisiID.Value = hf_HuroDivisiID.Value;
            lbldivID.Text = hf_divisiID.Value;
            SetDept(lbldivID.Text, ddlDept);
            SetLIne(pgc.getDropdownValue(ddlSection), ddlLine);
            lblProposedBy.Text = hf_empname.Value;
            txtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

            string date = txtDate.Text.Trim();
            //lblHoliday.Text = ifun.GetIsCalLibur(date).ToString();
            lblHoliday.Text = ifun.GetIsCalLibur(date, lblCaltypeID.Text).ToString();
            if (lblHoliday.Text == "True")
            {
                lblHoliday.Text = "1";
            }
            if (lblHoliday.Text.Trim() == "1")
            {
                lblHoliday.BackColor = System.Drawing.Color.AntiqueWhite;
                lblHoliday.Text = "Holiday OT";
            }
            else
            {
                lblHoliday.BackColor = System.Drawing.Color.Transparent;
                lblHoliday.Text = "Daily OT";
            }

            lblDuration.Text = SetTotalOT(txtstarttime, txtEndTime) + " Hours";
            panelList.Visible = false;
           //panelListSearch.Visible = false;

        }


    }
    protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
    {

        lblDepartment.Text = ddlDept.SelectedValue;
        SetSection(pgc.getDropdownValue(ddlDept), ddlSection);
        HilangkanFieldGV(hfAppTaskStep.Value, GV01);
    }

    protected void ddlSection_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetLIne(pgc.getDropdownValue(ddlSection), ddlLine);
        HilangkanFieldGV(hfAppTaskStep.Value, GV01);
        pgc.setControlClear(lblCalTypeName, lblCaltypeID);
    }
    protected void ddlLine_SelectedIndexChanged(object sender, EventArgs e)
    {
        line = PapuaInstancePebChart.GetInstancePEBMTLine;
        string lineid = pgc.getDropdownValue(ddlLine);
        pgc.setControlList(line.GetLineByParam("a.lineID", lineid, "="), "calTypeID", "catTypeName", ddCalTypeID);
        lblCalTypeName.Text = ddCalTypeID.SelectedItem.Text;
        lblCaltypeID.Text = ddCalTypeID.SelectedValue.ToString();
        HilangkanFieldGV(hfAppTaskStep.Value, GV01);


        if (!string.IsNullOrEmpty(txtDate.Text))
        {
            pgc.setControlClear(txtstarttime, txtEndTime, lblDuration, lblHoliday);
            string date = txtDate.Text.Trim();
            //lblHoliday.Text = ifun.GetIsCalLibur(date).ToString();
            lblHoliday.Text = ifun.GetIsCalLibur(date, lblCaltypeID.Text).ToString();
            if (lblHoliday.Text == "True")
            {
                lblHoliday.Text = "1";
            }
            if (lblHoliday.Text.Trim() == "1")
            {
                lblHoliday.BackColor = System.Drawing.Color.AntiqueWhite;
                lblHoliday.Text = "Holiday OT";
                lblHoliday.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                lblHoliday.BackColor = System.Drawing.Color.Transparent;
                lblHoliday.Text = "Daily OT";
                lblHoliday.ForeColor = System.Drawing.Color.Black;
            }
        }


    }
    protected void ddlShiftCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgc.setControlClear(txtstarttime, txtEndTime, lblDuration);
        lblDuration.Text = SetTotalOT(txtstarttime, txtEndTime) + " Hours";
        HilangkanFieldGV(hfAppTaskStep.Value, GV01);

    }
    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        if ((Convert.ToDateTime(txtDate.Text) < Convert.ToDateTime(lblDateFrom.Text)))
        {
            lblHoliday.Text = "Date is Not valid";
            lblHoliday.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            pgc.setControlClear(txtstarttime, txtEndTime, lblDuration, lblHoliday);
            string date = txtDate.Text.Trim();
            lblHoliday.Text = ifun.GetIsCalLibur(date, lblCaltypeID.Text).ToString();
            if (lblHoliday.Text == "True")
            {
                lblHoliday.Text = "1";
            }
            if (lblHoliday.Text.Trim() == "1")
            {
                lblHoliday.BackColor = System.Drawing.Color.AntiqueWhite;
                lblHoliday.Text = "Holiday OT";
                lblHoliday.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                lblHoliday.BackColor = System.Drawing.Color.Transparent;
                lblHoliday.Text = "Daily OT";
                lblHoliday.ForeColor = System.Drawing.Color.Black;
            }

        }

    }
    private void GetOTHourRule()
    {
        mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
        string PlanTimeFrom = txtDate.Text + " " + txtstarttime.Text;
        string PlanTimeTo = txtDate.Text + " " + txtEndTime.Text;
        string shift = pgc.getDropdownValue(ddlShiftCode);
        string section = pgc.getDropdownValue(ddlSection);
        string line = pgc.getDropdownValue(ddlLine);

        if (GV01.Rows.Count > 0)
        {
            DataTable dtTemp = mgr01.GetOverTime_HourRule(PlanTimeFrom, PlanTimeTo, shift, section, line);
            foreach (GridViewRow row in GV01.Rows)
            {
                TextBox txtPTimeFrom = (TextBox)row.FindControl("txtPTimeFrom");
                TextBox txtPTimeTo = (TextBox)row.FindControl("txtPTimeTo");
                Label lblPDuration = (Label)row.FindControl("lblPDuration");
                txtPTimeFrom.Text = PlanTimeFrom;
                txtPTimeTo.Text = PlanTimeTo;

                if (dtTemp.Rows.Count > 0)
                {
                    //OTReal,OTMaxRule,OTByRule,IsOut
                    //lblActualTimeFrom
                    //    lblActualTimeTo

                    lblPDuration.Text = dtTemp.Rows[0]["OTByRule"].ToString();
                    lblDuration.Text = lblPDuration.Text + " Hours";
                }
                else
                {
                    lblPDuration.Text = "";
                    lblDuration.Text = lblPDuration.Text + " Hours";
                }
            }
        }
    }
    protected void txtstarttime_TextChanged(object sender, EventArgs e)
    {


        lblDuration.Text = SetTotalOT(txtstarttime, txtEndTime) + " Hours";
        SetTimeforOT(GV01, txtstarttime.Text, txtEndTime.Text);

        //HilangkanFieldGV("1", GV01);
        HilangkanFieldGV(hfAppTaskStep.Value, GV01);
    }
    private void SetTimeforOT(GridView gv, string TimeFrom, string TimeTo)
    {
        int length = gv.Rows.Count;
        for (int i = 0; i < length; i++)
        {
            TextBox tbTimeFrom = (TextBox)gv.Rows[i].FindControl("txtPTimeFrom");
            TextBox tbTimeTo = (TextBox)gv.Rows[i].FindControl("txtPTimeTo");
            Label lbDuration = (Label)gv.Rows[i].FindControl("lblPDuration");
            tbTimeFrom.Text = TimeFrom;
            tbTimeTo.Text = TimeTo;
            lbDuration.Text = SetTotalOT(tbTimeFrom, tbTimeTo);
        }

    }
    protected void txtEndTime_TextChanged(object sender, EventArgs e)
    {
        SetTimeforOT(GV01, txtstarttime.Text, txtEndTime.Text);
        lblDuration.Text = SetTotalOT(txtstarttime, txtEndTime) + " Hours";
        lblDuration.Visible = true;
        HilangkanFieldGV(hfAppTaskStep.Value, GV01);
    }
    private string SetTotalOT(TextBox txtstarttime_s, TextBox txtEndTime_s)
    {
        string result = string.Empty;

        if (pgc.setAccessSave(txtDate, txtstarttime, ddlShiftCode, ddlSection))
        {
            mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
            string PlanTimeFrom = txtDate.Text + " " + txtstarttime_s.Text.Replace(".", ":");
            string PlanTimeTo = txtDate.Text + " " + txtEndTime_s.Text.Replace(".", ":");

            bool benar = pgc.checkDateTime(Convert.ToDateTime(PlanTimeFrom), Convert.ToDateTime(PlanTimeTo));

            if (benar)
            {

                DateTime x = Convert.ToDateTime(txtDate.Text).AddDays(1);
                PlanTimeTo = x.ToString("yyyy-MM-dd") + " " + txtEndTime_s.Text.Replace(".", ":");
            }

            string shift = pgc.getDropdownValue(ddlShiftCode);
            string section = pgc.getDropdownValue(ddlSection);
            string lineid = pgc.getDropdownValue(ddlLine);
            DataTable dtTemp = mgr01.GetOverTime_HourRule(PlanTimeFrom, PlanTimeTo, shift, section, lineid);
            //OTReal,OTMaxRule,OTByRule,IsOut
            if (dtTemp.Rows.Count > 0)
                result = dtTemp.Rows[0]["OTByRule"].ToString();
        }
        return result;
    }
    private void SetOTEmployee(GridView gv)
    {
        DataTable dtTemp = new DataTable();
        mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
        string dept = string.Empty;
        string section = string.Empty;
        string line = string.Empty;
        string date = txtDate.Text;
        string PlanTimeFrom = date + " " + txtstarttime.Text.Replace(".", ":");
        string PlanTimeTo = date + " " + txtEndTime.Text.Replace(".", ":");

        bool benar = pgc.checkDateTime(Convert.ToDateTime(PlanTimeFrom), Convert.ToDateTime(PlanTimeTo));
        if (benar)
        {
            DateTime x = Convert.ToDateTime(date).AddDays(1);
            PlanTimeTo = x.ToString("yyyy-MM-dd") + " " + txtEndTime.Text.Replace(".", ":");
        }

        string shift = string.Empty;
        string actualTimeFrom = string.Empty;
        string actualTimeTo = string.Empty;

        if (pgc.setAccessSave(ddlDept))
        {
            dept = pgc.getDropdownValue(ddlDept);
            if (pgc.setAccessSave(ddlSection))
            {
                section = pgc.getDropdownValue(ddlSection);
                if (pgc.setAccessSave(ddlLine))
                {
                    line = pgc.getDropdownValue(ddlLine);
                    if (pgc.setAccessSave(ddlShiftCode))
                    {
                        shift = pgc.getDropdownValue(ddlShiftCode);
                    }
                }
            }
        }

        dtTemp = mgr01.GetOverTimeEmployee(lbldivID.Text, dept, section, line, PlanTimeFrom, PlanTimeTo, shift, actualTimeFrom, actualTimeTo, lblOTForm.Text);
        BindDataGV(GV01, dtTemp);
    }
    private void SetOTEmployeeTanpaParam(GridView gv)
    {
        string deptid = string.Empty;
        string divisiid = string.Empty;
        DataTable dthed = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", HF_OTDTID.Value.ToString(), "", "=", "", "", "", "", ""));
        if (dthed.Rows.Count > 0)
        {
            divisiid = dthed.Rows[0]["divisiID"].ToString();
            deptid = dthed.Rows[0]["deptID"].ToString();
        }
        else
        {
            divisiid = lbldivID.Text;
            deptid = hf_deptID.Value;
        }

        DataTable dtTemp = new DataTable();
        mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
        string section = string.Empty;
        string line = string.Empty;
        string shift = string.Empty;
        string date = txtDate.Text;
        string PlanTimeFrom = date + " " + txtstarttime.Text.Replace(".", ":");
        string PlanTimeTo = date + " " + txtEndTime.Text.Replace(".", ":");

        bool benar = pgc.checkDateTime(Convert.ToDateTime(PlanTimeFrom), Convert.ToDateTime(PlanTimeTo));
        if (benar)
        {
            DateTime x = Convert.ToDateTime(date).AddDays(1);
            PlanTimeTo = x.ToString("yyyy-MM-dd") + " " + txtEndTime.Text.Replace(".", ":");
        }

        string actualTimeFrom = string.Empty;
        string actualTimeTo = string.Empty;

        if (pgc.setAccessSave(ddlSection))
        {
            if (pgc.setAccessSave(ddlShiftCode))
            {
                shift = pgc.getDropdownValue(ddlShiftCode);
            }
        }

        dtTemp = mgr01.GetOverTimeEmployee(divisiid, deptid, section, line, PlanTimeFrom, PlanTimeTo, shift, actualTimeFrom, actualTimeTo, lblOTForm.Text);
        if (dtTemp.Rows.Count > 0)
        {
            BindDataGV(gv, dtTemp);
        }
        else
        {
            WebHelper.MessageBoxAjaxs(UPUtama, "Application Already Exist !", false);
        }

    }
    private void SetClearData()
    {
        pgc.setControlClear(ddlLine, ddlSection, ddlShiftCode, txtDate, txtEndTime, txtstarttime, HF_OTDTID);
    }
    protected void txtPTimeFrom_TextChanged(object sender, EventArgs e)
    {


        TextBox t = (TextBox)sender;
        GridViewRow gvr = (GridViewRow)t.Parent.Parent;
        int i = gvr.RowIndex;


        TextBox txtPTimeTo = ((TextBox)gvr.Cells[0].FindControl("txtPTimeTo"));
        TextBox txtPTimeFrom = ((TextBox)gvr.Cells[0].FindControl("txtPTimeFrom"));
        Label lblPDuration = ((Label)gvr.Cells[0].FindControl("lblPDuration"));
        lblDuration.Text = "";
        if (pgc.setAccessSave(txtPTimeFrom, txtPTimeTo))
        {
            lblPDuration.Text = SetTotalOT(txtPTimeFrom, txtPTimeTo);
        }
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {


        TextBox t = (TextBox)sender;
        GridViewRow gvr = (GridViewRow)t.Parent.Parent;
        int i = gvr.RowIndex;


        TextBox txtPTimeTo = ((TextBox)gvr.Cells[0].FindControl("txtPTimeTo"));
        TextBox txtPTimeFrom = ((TextBox)gvr.Cells[0].FindControl("txtPTimeFrom"));
        Label lblPDuration = ((Label)gvr.Cells[0].FindControl("lblPDuration"));
        lblDuration.Text = "";
        if (pgc.setAccessSave(txtPTimeFrom, txtPTimeTo))
        {
            lblPDuration.Text = SetTotalOT(txtPTimeFrom, txtPTimeTo);
        }

    }

    #endregion

    #region HilangSembunyi
    private void sembunyiHF(GridView gv, string HiddenPro, string HiddenAg)
    {
        int length = gv.Rows.Count;
        for (int i = 0; i < length; i++)
        {
            HiddenField hfisproposed = (HiddenField)gv.Rows[i].FindControl("hfIsProposed");
            hfisproposed.Value = HiddenPro;
            if (HiddenPro == "0")
            {
                Button btnIsProposed = (Button)gv.Rows[i].FindControl("btnIsProposed");
                btnIsProposed.Visible = false;
            }
            else
            {
                Button btnIsProposed = (Button)gv.Rows[i].FindControl("btnIsProposed");
                btnIsProposed.Visible = true;
            }

            HiddenField hfisagree = (HiddenField)gv.Rows[i].FindControl("hfIsAgree");
            hfisagree.Value = HiddenAg;
            if (HiddenAg == "0") //|| string.IsNullOrEmpty(HiddenAg)
            {
                Button btnIsAgree = (Button)gv.Rows[i].FindControl("btnIsAgree");
                btnIsAgree.Visible = false;

                CheckBox chkCheck = (CheckBox)gv.Rows[i].FindControl("chkCheck");
                chkCheck.Enabled = true;
            }
            else
            {
                Button btnIsAgree = (Button)gv.Rows[i].FindControl("btnIsAgree");
                btnIsAgree.Visible = true;
            }
        }



    }
    private void sembunyiHF(GridView gv, string HiddenPro)
    {
        int length = gv.Rows.Count;
        for (int i = 0; i < length; i++)
        {
            HiddenField hfisproposed = (HiddenField)gv.Rows[i].FindControl("hfIsProposed");
            hfisproposed.Value = HiddenPro;
            if (HiddenPro == "0")
            {
                Button btnIsProposed = (Button)gv.Rows[i].FindControl("btnIsProposed");
                btnIsProposed.Visible = false;
            }
            else
            {
                Button btnIsProposed = (Button)gv.Rows[i].FindControl("btnIsProposed");
                btnIsProposed.Visible = true;
            }
        }
    }
    private void sembunyiHF(GridViewRowEventArgs e)
    {
        //int length = gv.Rows.Count;
        //for (int i = 0; i < length; i++)
        //{
        HiddenField hfisagree = (HiddenField)e.Row.FindControl("hfIsAgree");
        HiddenField hfisproposed = (HiddenField)e.Row.FindControl("hfIsProposed");

        if (hfisproposed.Value == "0")
        {
            Button btnIsProposed = (Button)e.Row.FindControl("btnIsProposed");
            btnIsProposed.Visible = false;
        }

        if (hfisagree.Value == null) //|| string.IsNullOrEmpty(hf_isAgree.Value)
        {
            Button btnIsAgree = (Button)e.Row.FindControl("btnIsAgree");
            btnIsAgree.Visible = false;

            //CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
            //chkCheck.Enabled = false;

            CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
            chkCheck.Enabled = true;
        }

        if (hfisagree.Value == "0") //|| string.IsNullOrEmpty(hf_isAgree.Value)
        {
            Button btnIsAgree = (Button)e.Row.FindControl("btnIsAgree");
            btnIsAgree.Visible = false;

            //CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
            //chkCheck.Enabled = false;

            CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
            chkCheck.Enabled = true;
        }
        else
        {
            Button btnIsAgree = (Button)e.Row.FindControl("btnIsAgree");
            btnIsAgree.Visible = true;

            CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
            chkCheck.Enabled = true;
        }




        //}

    }
    private void sembunyiHF(GridViewRowEventArgs e, bool tampil)
    {
        HiddenField hfisproposed = (HiddenField)e.Row.FindControl("hfIsProposed");

        if (hfisproposed.Value == "0")
        {
            Button btnIsProposed = (Button)e.Row.FindControl("btnIsProposed");
            btnIsProposed.Visible = false;
        }
        else
        {
            Button btnIsProposed = (Button)e.Row.FindControl("btnIsProposed");
            btnIsProposed.Visible = true;
        }

        CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
        chkCheck.Enabled = true;

    }
    private void gantiLabel()
    {
        int length = GV01.Rows.Count;
        for (int i = 0; i < length; i++)
        {
            TextBox TimeFrom = (TextBox)GV01.Rows[i].FindControl("txtPTimeFrom");
            TimeFrom.Visible = false;
            Label PTimeFrom = (Label)GV01.Rows[i].FindControl("lblPTimeFrom");
            PTimeFrom.Visible = true;

            TextBox TimeTo = (TextBox)GV01.Rows[i].FindControl("txtPTimeTo");
            TimeTo.Visible = false;
            Label PTimeTo = (Label)GV01.Rows[i].FindControl("lblPTimeTo");
            PTimeTo.Visible = true;
        }
    }

    private void panggilHilangkanField()
    {
        string apptask = hfAppTaskStep.Value.Trim();
        //DataTable dttemp = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", HF_OTDTID.Value.ToString(), "", "=", "", "", "", "", ""));
        //apptask = dttemp.Rows[0]["AppTaskID"].ToString();
        hf_taskID.Value = apptask;

        if (apptask == "1")
        {
            HilangkanFieldGV("1", GV01);
        }
        else if (apptask == "2" || apptask == "3")
        {
           
            //btnReject.Visible = false;
            gantiLabel();
            HilangkanFieldGV("2", GV01);
        }
        else
        {
            HilangkanFieldGV("0", GV01);
        }

    }
    private void HilangkanFieldGV(string appTaskStep, GridView gv)
    {
        if (hfAppTaskStep.Value == "1")
        {
            if (gv == GV01)
            {
                pgc.GVColumnVisibility(GV01, new[] { "IsProposed", "IsAgree" }, new[] { "Adj Drtn", "Remark", "Actual T.From", "Actual T.To", "Act Drtn", "PDW", "Result Hour", "Formula Hour", "Attd" });
            }
            //else
            //{
            //    pgc.GVColumnVisibility(GV02, new[] { "" }, new[] { "Adj Drtn", "Remark", "Actual T.From", "Actual T.To", "IsAgree", "IsProposed", "Act Drtn", "PDW", "Result Hour", "Formula Hour", "Attd" });
            //}

            if (HFOTTypeForm.Value == "0" && gv == GV01)
            {
                lblDuration.Visible = false;
                txtstarttime.Visible = false;
                txtEndTime.Visible = false;
                lbl.Visible = false;
                lbltitik.Visible = false;
                lbltitikApp.Visible = false;
                lblto.Visible = false;
                pgc.GVColumnVisibility(GV01, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Adj Drtn", "Remark", "IsAgree", "PDW", "Result Hour", "Formula Hour", "Attd" }, new[] { "IsProposed", "Plan T.From", "Plan T.To", "Plan Drtn" });
            }
            //else if (HFOTTypeForm.Value == "0" && gv == GV02)
            //{
            //    txtstarttime.Visible = false;
            //    txtEndTime.Visible = false;
            //    lbl.Visible = false;
            //    lbltitik.Visible = false;
            //    lbltitikApp.Visible = false;
            //    lblDuration.Visible = false;
            //    lblto.Visible = false;
            //    pgc.GVColumnVisibility(GV02, new[] { "Adj Drtn", "Remark", "Actual T.From", "Actual T.To", "IsAgree", "IsProposed", "Act Drtn", "PDW", "Result Hour", "Formula Hour", "Attd" }, new[] { "IsProposed", "Plan T.From", "Plan T.To", "Plan Drtn" });
            //}
        }
        else if (hfAppTaskStep.Value == "2")
        {
            if (gv == GV01)
            {
                pgc.GVColumnVisibility(GV01, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Adj Drtn", "Remark", "IsProposed", "PDW", "Result Hour", "Formula Hour", "Attd" }, new[] { "IsAgree" });
            }
            else
            {
                pgc.GVColumnVisibility(GV01, new[] { "", "" }, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Remark", "Adj Drtn", "IsProposed", "IsAgree", "PDW", "Result Hour", "Formula Hour", "Attd" });
            }

            if (HFOTTypeForm.Value == "0" && gv == GV01)
            {
                lblDuration.Visible = false;
                txtstarttime.Visible = false;
                txtEndTime.Visible = false;
                lbl.Visible = false;
                lbltitik.Visible = false;
                lbltitikApp.Visible = false;
                lblto.Visible = false;
                pgc.GVColumnVisibility(GV01, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Adj Drtn", "Remark", "IsAgree", "PDW", "PDW", "Result Hour", "Formula Hour", "Attd" }, new[] { "IsProposed", "Plan T.From", "Plan T.To", "Plan Drtn" });
            }
            //else if (HFOTTypeForm.Value == "0" && gv == GV02)
            //{
            //    txtstarttime.Visible = false;
            //    txtEndTime.Visible = false;
            //    lbl.Visible = false;
            //    lbltitik.Visible = false;
            //    lbltitikApp.Visible = false;
            //    lblDuration.Visible = false;
            //    lblto.Visible = false;
            //    pgc.GVColumnVisibility(GV02, new[] { "Adj Drtn", "Remark", "Actual T.From", "Actual T.To", "IsAgree", "IsProposed", "Act Drtn", "PDW", "Result Hour", "Formula Hour", "Attd" }, new[] { "IsProposed", "Plan T.From", "Plan T.To", "Plan Drtn" });
            //}

        }
        else if (HFOTTypeForm.Value == "0")
        {
            pgc.GVColumnVisibility(GV01, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Adj Drtn", "Remark", "PDW", "Result Hour", "Formula Hour", "Attd" }, new[] { "IsProposed", "Plan T.From", "Plan T.To", "Plan Drtn", "IsAgree" });
        }
        else
        {
            if (gv == GV01)
            {
                pgc.GVColumnVisibility(GV01, new[] { "IsProposed" }, new[] { "Act Drtn", "Remark", "Actual T.From", "Actual T.To", "Adj Drtn", "PDW", "Result Hour", "Formula Hour", "IsAgree", "Attd" });
            }
            //else
            //{
            //    pgc.GVColumnVisibility(GV02, new[] { "IsProposed" }, new[] { "Act Drtn", "Remark", "Actual T.From", "Actual T.To", "IsProposed", "Adj Drtn", "PDW", "Result Hour", "Formula Hour", "IsAgree", "Attd" });
            //}
        }
    }
    #endregion

    #region BIND
    private void BindForm()
    {
        string appstep = string.Empty;
        if (hf_modeView.Value.ToString() == blank)
        {
            SetClearData();
            SetShiftCode(ddlShiftCode);
            SetOTForm();
            btnSearch0.Visible = true;
        }
        else
        {
            //HF_OTDTID.Value = Request.QueryString["ParamBEI"].ToString();
            //HF_OTDTID.Value = Encoding.Unicode.GetString(Convert.FromBase64String(HF_OTDTID.Value));

            //HFOTTypeForm.Value = Request.QueryString["paramBEI2"].ToString();
            //HFOTTypeForm.Value = Encoding.Unicode.GetString(Convert.FromBase64String(HFOTTypeForm.Value));


            if (HF_OTDTID.Value != null)
            {
                //if (Session["OTTypeForm"] != null)
                //    HFOTTypeForm.Value = Session["OTTypeForm"].ToString();

                if (HFOTTypeForm.Value == "0")
                {
                    HFOTTypeForm.Value = "0";
                    lblOTForm.Text = HFOTTypeForm.Value;
                    panelForm.GroupingText = "OT Abnormal Form";
                    //lblHeader.InnerText = "OT Abnormal List ";
                    //GeneralTitleHeader.setCustomTitle("OT Abnormal List");
                    lbltitik.Text = "OT Abnormal List";
                }
                else
                {
                    HFOTTypeForm.Value = "1";
                    lblOTForm.Text = HFOTTypeForm.Value;
                    panelForm.GroupingText = "OT Form";
                    //lblHeader.InnerText = "OT Normal List";
                   // GeneralTitleHeader.setCustomTitle("OT Normal List");
                    lbltitik.Text = "OT Normal List";
                }
            }



            if (hf_modeView.Value.ToString() == tbl01)
            {
                disable();
                btnSave.Visible = false;
                pgc.setControlVisible(true, btnUpdate, btnSubmit, panelList,   lblnextApp, lbltitikApp);
              //  setSuperior();
            }
            else if (hf_modeView.Value.ToString() == tbl02)
            {
                disable();
                btnSave.Visible = false;
                pgc.setControlEnabled(false, txtTask, txtstarttime, txtEndTime, txtTask, txtstarttime, txtEndTime);
                pgc.setControlVisible(true, panelList, lblnextApp, lbltitikApp);
                //BindHistApproval();
                //pnlHistoryApproval.Visible = true;
                //UPHistApproval.Update();
                pgc.GVColumnVisibility(GV01, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Adj Drtn", "Remark", "IsProposed" }, new[] { "IsAgree", "Check" });


            }
            else if (hf_modeView.Value.ToString() == tbl03)
            {
                disable();
                txtstarttime.Enabled = false;
                txtEndTime.Enabled = false;
                btnSave.Visible = false;
                pgc.setControlVisible(true, btnUpdate, panelList, btnApprove, btnReject);
                //BindHistApproval();
                //pnlHistoryApproval.Visible = true;
                //UPHistApproval.Update();
                lblnextApp.Visible = true;
                lbltitikApp.Visible = true;
                setSuperior();


            }
            else if (hf_modeView.Value.ToString() == tbl04)
            {
                disable();
                btnUpdate.Visible = true;
                txtTask.Enabled = false;
                txtstarttime.Enabled = false;
                txtEndTime.Enabled = false;
                btnSave.Visible = false;
                panelList.Visible = true;
                //BindHistApproval();
                //pnlHistoryApproval.Visible = true;
                //UPHistApproval.Update();
                lblnextApp.Visible = true;
                lbltitikApp.Visible = true;
                setSuperior();

            }
            else if (hf_modeView.Value.ToString() == tbl05)
            {
                disable();
                txtTask.Enabled = false;
                txtstarttime.Enabled = false;
                txtEndTime.Enabled = false;
                btnSave.Visible = false;
                panelList.Visible = true;
                //BindHistApproval();
                //pnlHistoryApproval.Visible = true;
                //UPHistApproval.Update();
                //lblnextApp.Visible = true;
                //lbltitikApp.Visible = true;
                //setSuperior();
            }
            else if (hf_modeView.Value.ToString() == tbl06)
            {
                disable();
                txtTask.Enabled = false;
                txtstarttime.Enabled = false;
                txtEndTime.Enabled = false;
                btnSave.Visible = false;
                panelList.Visible = true;
                //BindHistApproval();
                //pnlHistoryApproval.Visible = true;
                //UPHistApproval.Update();
                UPpriod.Visible = false;
                //lblnextApp.Visible = true;
                //lbltitikApp.Visible = true;
                //setSuperior();
            }


            DataTable dttemp = new DataTable();
            DataTable dtica = new DataTable();
            dttemp = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", HF_OTDTID.Value.ToString(), "", "=", "", "", "", "", ""));
            if (dttemp.Rows.Count > 0)
            {
                HF_OTDTID.Value = dttemp.Rows[0]["OTdtID"].ToString();
                lblrefID.Text = hf_menuID.Value + "-" + HF_OTDTID.Value + " ";
                lblDivision.Text = dttemp.Rows[0]["divisi"].ToString();
                lbldivID.Text = dttemp.Rows[0]["divisiID"].ToString();
                lblDepartment.Text = dttemp.Rows[0]["deptID"].ToString();
                hf_deptemail.Value = dttemp.Rows[0]["dept"].ToString();
                txtTask.Text = dttemp.Rows[0]["OTTaskDesc"].ToString();
                lblProposedBy.Text = dttemp.Rows[0]["InsertByName"].ToString();
                lbldeptcadangan.Text = dttemp.Rows[0]["dept"].ToString();
                ddlDept.Visible = false;
                lbldeptcadangan.Visible = true;
                SetShiftCode(ddlShiftCode);
                ddlShiftCode.SelectedValue = dttemp.Rows[0]["shiftCode"].ToString();
                SetSection(lblDepartment.Text, ddlSection);
                ddlSection.SelectedValue = dttemp.Rows[0]["sectionid"].ToString();
                SetLIne(ddlSection.SelectedValue, ddlLine);
                ddlLine.SelectedValue = dttemp.Rows[0]["lineID"].ToString();
                txtDate.Text = dttemp.Rows[0]["requestDate"].ToString();
                appstep = dttemp.Rows[0]["AppStep"].ToString();
                hfAppStep.Value = appstep;
                hfAppTaskStep.Value = dttemp.Rows[0]["AppTaskStep"].ToString();
                hfAppTaskStepName.Value = dttemp.Rows[0]["AppTaskStepName"].ToString();
                lblOTForm.Text = dttemp.Rows[0]["normal_bool"].ToString();
                if (lblOTForm.Text == "False")
                {
                    lblOTForm.Text = "0";
                }
                else
                {
                    lblOTForm.Text = "1";
                }

                hf_proposerNo.Value = dttemp.Rows[0]["InsertBy"].ToString();
                lblHoliday.Text = dttemp.Rows[0]["holiday_bool"].ToString();
                if (lblHoliday.Text == "True")
                {
                    lblHoliday.Text = "1";
                }

                if (lblHoliday.Text.Trim() == "1")
                {
                    lblHoliday.BackColor = System.Drawing.Color.AntiqueWhite;
                    lblHoliday.Text = "Holiday OT";
                }
                else
                {
                    lblHoliday.BackColor = System.Drawing.Color.Transparent;
                    lblHoliday.Text = "Daily OT";
                }

                hf_taskID.Value = getOTTaskid(hfAppTaskStep.Value);
                setSuperior();
                if (lblProposedBy.Text.Trim() != hf_empname.Value.Trim())
                    btnSubmit.Enabled = false;

                string task = hf_taskID.Value;
                //dtica = ica.GetApprovalGeneralInfo(hf_menuID.Value, getTaskID(hf_menuID.Value.Trim()), appstep, hf_proposerNo.Value);
                string error = string.Empty;
                dtica = ica.GetApprovalGeneralInfo(out error, hf_menuID.Value, task, appstep, hf_proposerNo.Value);
                //hfFingerPrint.Value = dtica.Rows[0]["isFingerPrint"].ToString();
                hfFingerPrint.Value = "1";


                PapuaBLCommonParamExt pcpe = new PapuaBLCommonParamExt();
                pcpe.ParamField1 = "A.OTdtID";
                pcpe.ParamFieldData1 = HF_OTDTID.Value.ToString();
                pcpe.Operrator1 = "=";
                DataTable dtdet = inst_detail.GetOverTime_dtDetail(pcpe);

                int length = dtdet.Rows.Count;
                for (int i = 0; i < length; i++)
                {
                    txtstarttime.Text = dtdet.Rows[i]["planFromTime"].ToString().Replace(".", ":");
                    txtEndTime.Text = dtdet.Rows[i]["planToTime"].ToString().Replace(".", ":");
                    lblDuration.Visible = true;
                    lblDuration.Text = dtdet.Rows[i]["planDuration"] + " Hours".ToString();
                    
                }
                BindDataGV(GV01, dtdet);
            }



            if (hf_modeView.Value.ToString() == tbl02)
            {
                LbSuperior.Visible = true;
                lbltitikApp.Visible = true;
              //  LbSuperior.Text = dttemp.Rows[0]["AppStepEmpName"].ToString();
                lblnextApp.Text = dttemp.Rows[0]["AppStepTitle"].ToString();

                panggilHilangkanField();
            }
            else if (hf_modeView.Value.ToString() == tbl05 || hf_modeView.Value.ToString() == tbl06)
            {
                LbSuperior.Visible = false;
                panggilHilangkanField();
            }
            else
            {
                panggilHilangkanField();
                string task = getOTTaskid(hfAppTaskStep.Value);
                string error = string.Empty;
                //DataTable appInfo = ica.GetApprovalGeneralInfo(hf_menuID.Value.ToString(), task, appstep, hf_proposerNo.Value);
                string nextStep = (ica.GetNextApprovalStep(out error, hf_menuID.Value.ToString(), task, hf_proposerNo.Value, appstep)).ToString();
                DataTable dt = new DataTable();
                dt = ica.GetSuperiorByApprovalSetup(out error, hf_menuID.Value.ToString(), task, nextStep, hf_empno.Value.ToString());

                if (dt.Rows.Count > 1)
                {
                    DdSuperior.Visible = true;
                    DdSuperior.DataSource = dt;
                    DdSuperior.DataValueField = "empNo";
                    DdSuperior.DataTextField = "empName";
                    hf_SuperiorId.Value = dt.Rows[0]["empNo"].ToString();
                    DdSuperior.DataBind();
                    lblnextApp.Visible = true;
                    lbltitikApp.Visible = true;
                    lblnextApp.Text = dtica.Rows[0]["NextTitleNameAppr"].ToString();
                }
                else
                {
                    if (nextStep == "1000")
                    {
                        lblnextApp.Visible = false;
                        LbSuperior.Visible = false;
                        lbltitikApp.Visible = false;
                        hf_taskID.Value = getOTTaskid(hfAppTaskStep.Value);
                    }
                    else
                    {
                        lblnextApp.Visible = true;
                        lbltitikApp.Visible = true;
                        lblnextApp.Text = dtica.Rows[0]["NextTitleNameAppr"].ToString();
                        LbSuperior.Visible = true;
                        //LbSuperior.Text = dt.Rows[0]["empName"].ToString();
                        //hf_SuperiorId.Value = dt.Rows[0]["empNo"].ToString();
                    }
                }

                if (hf_modeView.Value.ToString() == tbl01)
                {
                    //pnlHistoryApproval.Visible = false;
                    panggilHilangkanField();
                }

                if (hf_modeView.Value.ToString() == tbl03)
                {
                    if (hfAppTaskStep.Value == "1")
                    {
                        btnUpdate.Visible = true;
                    }
                    else if (hfAppTaskStep.Value == "2")
                    {
                        btnReject.Visible = false;
                    }
                }


                if (hf_modeView.Value.ToString() == tbl04)
                {
                    panggilHilangkanField();
                    if (hf_proposerNo.Value == hf_empno.Value)
                    {
                        lblnextApp.Text = "Next Approval";
                        pgc.setControlVisible(true, LbSuperior, lbltitikApp, btnApprove);
                    }
                    else
                    {
                        btnUpdate.Visible = false;
                        pgc.setControlVisible(false, LbSuperior, lbltitikApp, btnApprove, btnReject, lblnextApp);
                        txtTask.Enabled = true;
                    }
                }
            }
        }

    }


    //private void BindHistApproval()
    //{
    //    pnlHistoryApproval.Visible = true;
    //    DataTable dthistori = inst_header.GetOverTime_dtHeaderApprovalHistory(HF_OTDTID.Value.Trim());
    //    this.BindDataGV(Gv_HistoryApproval, dthistori);
    //    this.Gv_HistoryApproval.Caption = "";
    //    this.Gv_HistoryApproval.HeaderStyle.ForeColor = System.Drawing.Color.White;
    //    UPHistApproval.Update();
    //}
    private void BindDropDown(DropDownList dd, DataTable dt, string id, string name)
    {
        pgc.setControlClear(dd);

        dd.DataSource = dt;
        dd.DataValueField = id;
        dd.DataTextField = name;
        dd.DataBind();
        ListItem litem = new ListItem();
        litem.Text = "";
        litem.Value = "0";
        dd.Items.Insert(0, litem);

    }
    private void BindDataGV(GridView gv, DataTable dtTemp)
    {
        gv.DataSource = dtTemp;
        gv.DataBind();
        gv.Caption = "Total " + dtTemp.Rows.Count.ToString() + " Rows Displayed";
        gv.HeaderStyle.ForeColor = System.Drawing.Color.White;
        dtTemp.Dispose();

    }

    #endregion

    #region PrivateDatatable
    private DataTable dtTemfsdfsdf()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("data", typeof(string));
        dt.Columns.Add("value", typeof(string));
        DataRow dr = dt.NewRow();
        dr[0] = "makan";
        dr[1] = "nasi";
        dt.Rows.Add(dr);
        return dt;
    }
    private DataTable GridviewChecked(GridView gv, bool IsChecked, bool IsSearh)
    {
        DataTable dtTemp = new DataTable();
        bool IsAgree = true;
        dtTemp.Columns.AddRange(new DataColumn[] { new DataColumn("lblDetailsID", typeof(string)), new DataColumn("lblEmpNo", typeof(string)), new DataColumn("hfIsAgree", typeof(string))
            , new DataColumn("txtPTimeFrom", typeof(string)), new DataColumn("txtPTimeTo", typeof(string)), new DataColumn("lblPDuration", typeof(string))
             , new DataColumn("chkCheck", typeof(string)), new DataColumn("txtAdjDuration", typeof(string)), new DataColumn("txtAdjRemark", typeof(string))
            ,  new DataColumn("lblActualTimeFrom", typeof(string)), new DataColumn("lblActualTimeTo", typeof(string))
            ,  new DataColumn("lblActualDuration", typeof(string))});
        for (int i = 0; i < gv.Rows.Count; i++)
        {

            if (IsChecked)
            {
                CheckBox cb = (CheckBox)gv.Rows[i].FindControl("chkCheck");
                Label lblActualDuration = (Label)gv.Rows[i].FindControl("lblActualDuration");
                TextBox txtAdjDuration = (TextBox)gv.Rows[i].FindControl("txtAdjDuration");

                if (!cb.Checked)
                {
                    IsAgree = false;
                }
                else
                {
                    IsAgree = true;
                }

                //if (HFOTTypeForm.Value == "0")
                //{
                //    if (cb.Checked && string.IsNullOrEmpty(lblActualDuration.Text))
                //    {
                //        IsAgree = false;
                //    }

                //    if (cb.Checked && !string.IsNullOrEmpty(txtAdjDuration.Text))
                //    {
                //        IsAgree = true;
                //    }

                //}

            }
            if (IsAgree)
            {
                string lblDetailsID = string.Empty;
                string lblEmpNo = string.Empty;
                string hfIsAgree = string.Empty;
                string lblPDuration = string.Empty;
                string txtPTimeFrom = string.Empty;
                string txtPTimeTo = string.Empty;
                string txtAdjDuration = string.Empty;
                string txtAdjRemark = string.Empty;
                string lblActualTimeFrom = string.Empty;
                string lblActualTimeTo = string.Empty;
                string lblActualDuration = string.Empty;
                lblDetailsID = ((Label)gv.Rows[i].FindControl("lblDetailsID")).Text.Trim();
                lblEmpNo = ((Label)gv.Rows[i].FindControl("lblEmpNo")).Text.Trim();
                hfIsAgree = ((HiddenField)gv.Rows[i].FindControl("hfIsAgree")).Value.Trim();
                lblPDuration = ((Label)gv.Rows[i].FindControl("lblPDuration")).Text.Trim();
                txtPTimeFrom = ((TextBox)gv.Rows[i].FindControl("txtPTimeFrom")).Text.Trim();
                txtPTimeTo = ((TextBox)gv.Rows[i].FindControl("txtPTimeTo")).Text.Trim();
                lblActualTimeFrom = ((Label)gv.Rows[i].FindControl("lblActualTimeFrom")).Text.Trim();
                lblActualTimeTo = ((Label)gv.Rows[i].FindControl("lblActualTimeTo")).Text.Trim();
                lblActualDuration = ((Label)gv.Rows[i].FindControl("lblActualDuration")).Text.Trim();
                if (!IsSearh)
                {
                    txtAdjDuration = ((TextBox)gv.Rows[i].FindControl("txtAdjDuration")).Text.Trim();
                    txtAdjRemark = ((TextBox)gv.Rows[i].FindControl("txtAdjRemark")).Text.Trim();
                }
                DataRow dr = dtTemp.NewRow();
                dr["lblDetailsID"] = lblDetailsID;
                dr["lblEmpNo"] = lblEmpNo;
                dr["hfIsAgree"] = hfIsAgree;
                dr["lblPDuration"] = lblPDuration;
                dr["txtPTimeFrom"] = txtPTimeFrom;
                dr["txtPTimeTo"] = txtPTimeTo;
                dr["txtAdjDuration"] = txtAdjDuration;
                dr["txtAdjRemark"] = txtAdjRemark;
                dr["lblActualTimeFrom"] = lblActualTimeFrom;
                dr["lblActualTimeTo"] = lblActualTimeTo;
                dr["lblActualDuration"] = lblActualDuration;
                dtTemp.Rows.Add(dr);

            }
        }
        return dtTemp;
    }
    private DataTable GridviewActual(DataTable dt)
    {
        DataTable dtTemp = new DataTable();
        dtTemp.Columns.AddRange(new DataColumn[] { new DataColumn("lblDetailsID", typeof(string)), new DataColumn("lblEmpNo", typeof(string)), new DataColumn("hfIsAgree", typeof(string))
            , new DataColumn("txtPTimeFrom", typeof(string)), new DataColumn("txtPTimeTo", typeof(string)), new DataColumn("lblPDuration", typeof(string))
             , new DataColumn("chkCheck", typeof(string)), new DataColumn("txtAdjDuration", typeof(string)), new DataColumn("txtAdjRemark", typeof(string))
            ,  new DataColumn("lblActualTimeFrom", typeof(string)), new DataColumn("lblActualTimeTo", typeof(string))
            ,  new DataColumn("lblActualDuration", typeof(string))});
        for (int i = 0; i < GV01.Rows.Count; i++)
        {
            string lblDetailsID = string.Empty;
            string lblEmpNo = string.Empty;
            string hfIsAgree = string.Empty;
            string lblPDuration = string.Empty;
            string txtPTimeFrom = string.Empty;
            string txtPTimeTo = string.Empty;
            string txtAdjDuration = string.Empty;
            string txtAdjRemark = string.Empty;
            string lblActualTimeFrom = string.Empty;
            string lblActualTimeTo = string.Empty;
            string lblActualDuration = string.Empty;
            lblDetailsID = ((Label)GV01.Rows[i].FindControl("lblDetailsID")).Text.Trim();
            lblEmpNo = ((Label)GV01.Rows[i].FindControl("lblEmpNo")).Text.Trim();
            hfIsAgree = ((HiddenField)GV01.Rows[i].FindControl("hfIsAgree")).Value.Trim();
            lblPDuration = ((Label)GV01.Rows[i].FindControl("lblPDuration")).Text.Trim();
            txtPTimeFrom = ((TextBox)GV01.Rows[i].FindControl("txtPTimeFrom")).Text.Trim();
            txtPTimeTo = ((TextBox)GV01.Rows[i].FindControl("txtPTimeTo")).Text.Trim();
            lblActualTimeFrom = ((Label)GV01.Rows[i].FindControl("lblActualTimeFrom")).Text.Trim();
            lblActualTimeTo = ((Label)GV01.Rows[i].FindControl("lblActualTimeTo")).Text.Trim();
            lblActualDuration = ((Label)GV01.Rows[i].FindControl("lblActualDuration")).Text.Trim();
            txtAdjDuration = ((TextBox)GV01.Rows[i].FindControl("txtAdjDuration")).Text.Trim();
            txtAdjRemark = ((TextBox)GV01.Rows[i].FindControl("txtAdjRemark")).Text.Trim();

            DataRow dr = dtTemp.NewRow();
            dr["lblDetailsID"] = lblDetailsID;
            dr["lblEmpNo"] = lblEmpNo;
            dr["hfIsAgree"] = hfIsAgree;
            dr["lblPDuration"] = lblPDuration;
            dr["txtPTimeFrom"] = txtPTimeFrom;
            dr["txtPTimeTo"] = txtPTimeTo;
            dr["txtAdjDuration"] = txtAdjDuration;
            dr["txtAdjRemark"] = txtAdjRemark;
            dr["lblActualTimeFrom"] = lblActualTimeFrom;
            dr["lblActualTimeTo"] = lblActualTimeTo;
            dr["lblActualDuration"] = lblActualDuration;
            dtTemp.Rows.Add(dr);
        }
        return dtTemp;
    }

    private DataTable IdAgree(GridView gv, bool agree)
    {
        int length = gv.Rows.Count;
        DataTable dttemp = new DataTable();
        dttemp.Columns.Add("id");
        for (int i = 0; i < length; i++)
        {
            DataRow dr = dttemp.NewRow();
            CheckBox cb = (CheckBox)gv.Rows[i].FindControl("chkCheck");
            if (agree)
            {
                if (cb.Checked)
                {
                    string id = ((Label)gv.Rows[i].FindControl("lblDetailsID")).Text.Trim();
                    dr["id"] = id;
                    dttemp.Rows.Add(dr);
                }
            }
            else
            {
                if (!cb.Checked)
                {
                    string id = ((Label)gv.Rows[i].FindControl("lblDetailsID")).Text.Trim();
                    dr["id"] = id;
                    dttemp.Rows.Add(dr);
                }
            }
        }
        return dttemp;
    }

    private DataTable IsAgree(GridView gv)//, bool hfagree)
    {
        int length = gv.Rows.Count;
        DataTable dttemp = new DataTable();
        dttemp.Columns.Add("id");
        for (int i = 0; i < length; i++)
        {
            DataRow dr = dttemp.NewRow();
            HiddenField hf = (HiddenField)gv.Rows[i].FindControl("hfIsAgree");
            CheckBox cb = (CheckBox)gv.Rows[i].FindControl("chkCheck");
            if (hf.Value != "1" || !cb.Checked)
            {

                string id = ((Label)gv.Rows[i].FindControl("lblDetailsID")).Text.Trim();
                dr["id"] = id;
                dttemp.Rows.Add(dr);

            }
            //else
            //{
            //    if (hf.Value == "0")
            //    {
            //        string id = ((Label)gv.Rows[i].FindControl("lblDetailsID")).Text.Trim();
            //        dr["id"] = id;
            //        dttemp.Rows.Add(dr);
            //    }
            //}
        }
        return dttemp;
    }

    private bool checkRejectOTEmployee(DataTable dttemp)
    {
        bool result = false;


        List<PapuaIdentityID> litem = new List<PapuaIdentityID>();
        for (int i = 0; i < dttemp.Rows.Count; i++)
        {
            PapuaIdentityID pid = new PapuaIdentityID();
            pid.ID = dttemp.Rows[i]["id"].ToString();
            litem.Add(pid);
        }

        result = inst_detail.DeleteByOverTime_dtDetailID(hf_empno.Value, litem);
        return result;
    }
    #endregion

    #region GetSetSuperior
    private string getOTTaskid(string apptaskstep)
    {
        string result = string.Empty;
        string error = string.Empty;

        string deptId = lblDepartment.Text;
        if (!string.IsNullOrEmpty(deptId))
        {
            if (apptaskstep == "1")
            {
                DataTable dtCheck = inst_detail.GetDeptProduction(deptId);
                if (dtCheck.Rows[0][0].ToString().Trim() == "True")
                {
                    result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "OT-Production").ToString();
                }
                else
                {
                    result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "OT-Non Production").ToString();
                }

                //special position manager pc karena lintas dept approval ( non prod ke prod)
                //if (hf_HuroDeptID.Value == "9" && hf_userlevel.Value == "3")
                //{
                //    result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "OT-Production").ToString();
                //}


            }
            else if (apptaskstep == "2")
            {
                result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "OT-Verify").ToString();
            }
        }

        //if (apptaskstep == "1")
        //{
        //    if (lblHoliday.Text == "Daily OT")
        //    {
        //        result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "BEFORE-OT Daily").ToString();
        //    }
        //    else
        //    {
        //        result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "BEFORE-OT Holiday").ToString();
        //    }

        //}
        //else if (apptaskstep == "2")
        //{
        //    if (lblHoliday.Text == "Daily OT")
        //    {
        //        result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "AFTER-OT Daily").ToString();
        //    }
        //    else
        //    {
        //        result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "AFTER-OT Holiday").ToString();
        //    }
        //}

        //if (HFOTTypeForm.Value == "0")
        //{
        //    if (lblHoliday.Text == "Daily OT")
        //    {

        //        result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "AFTER-OT Daily").ToString();
        //    }
        //    else
        //    {
        //        result = ica.GetTaskIDByTaskName(out error, hf_menuID.Value.ToString(), "AFTER-OT Holiday").ToString();
        //    }
        //}

        return result;
    }
    private string getTaskID(string taskid)
    {
        string error = string.Empty;
        ICommon_Approval ica = InstanceCommonApproval.GetCommon_Approval;
        DataTable dtTemp = new DataTable();
        string taskID = string.Empty;

        dtTemp = ica.GetTaskByMenuID(out error, taskid);
        if (dtTemp.Rows.Count != 0)
            taskID = dtTemp.Rows[0]["AppTaskID"].ToString();
        return taskID;
    }
    private string getNextStepApp(string MenuID, string taskID, string empNo, string appstep)
    {
        string nextStep = string.Empty;
        string error = string.Empty;
        nextStep = ica.GetNextApprovalStep(out error, MenuID, taskID, empNo, appstep).ToString();
        return nextStep;

    }
    private void setSuperior()
    {
        string error = string.Empty;
        string curStep = (ica.GetNextApprovalStep(out error, hf_menuID.Value, hf_taskID.Value, hf_empno.Value, hfAppStep.Value)).ToString();

        DataTable dtSuperior = new DataTable();
        dtSuperior = ica.GetSuperiorByApprovalSetup(out error, hf_menuID.Value, hf_taskID.Value, curStep, hf_empno.Value);

        if (dtSuperior.Rows.Count > 0)
        {
            DdSuperior.Visible = true;
            DdSuperior.DataSource = dtSuperior;
            DdSuperior.DataValueField = "empNo";
            DdSuperior.DataTextField = "empName";
            hf_SuperiorId.Value = dtSuperior.Rows[0]["empNo"].ToString();
            DdSuperior.DataBind();

        }
        else
        {
            if (curStep == "1000")
            {
                string isFinish = "1";
                lblnextApp.Visible = false;
                lbltitikApp.Visible = false;
                hf_SuperiorId.Value = hf_empno.Value.ToString();
            }
            else
            {
                //LbSuperior.Visible = true;
                //LbSuperior.Text = dtSuperior.Rows[0]["empName"].ToString();
                //hf_SuperiorId.Value = dtSuperior.Rows[0]["empNo"].ToString();
            }
        }
    }
    protected void DdSuperior_SelectedIndexChanged(object sender, EventArgs e)
    {
        hf_SuperiorId.Value = DdSuperior.SelectedValue;
    }
    private bool ApproveReject(bool Approve)
    {
        List<PapuaBLCommonApprovalExt> list_OT = new List<PapuaBLCommonApprovalExt>();
        bool result = false;
        if (Approve)
        {
            list_OT.Add(SetBLCommonApproval(true));
            if (HFOTTypeForm.Value == "0")
            {
                result = inst_header.ApproveRejectOverTime_dtHeaderAbNormal(list_OT);
            }
            else
            {
                result = inst_header.ApproveRejectOverTime_dtHeader(list_OT);
            }
        }

        else
        {
            list_OT.Add(SetBLCommonApproval(false));
            if (HFOTTypeForm.Value == "0")
            {
                result = inst_header.ApproveRejectOverTime_dtHeaderAbNormal(list_OT);
            }
            else
            {
                result = inst_header.ApproveRejectOverTime_dtHeader(list_OT);
            }
        }
        return result;
    }
    private PapuaBLCommonApprovalExt SetBLCommonApproval(bool isApprove)
    {
        string nextStep = string.Empty;
        string isFinish = string.Empty;
        string approve = "0";
        string task = string.Empty;
        string tasknext = string.Empty;

        PapuaBLCommonApprovalExt pbc = new PapuaBLCommonApprovalExt();
        if (isApprove == true)
        {
            hf_taskID.Value = getOTTaskid(hfAppTaskStep.Value); //aa
           // task = hf_taskID.Value;
            nextStep = getNextStepApp(hf_menuID.Value.Trim(), hf_taskID.Value, hf_empno.Value.Trim(), hfAppStep.Value);
            approve = "1";
        }

        if (nextStep == "1000")
        {
            isFinish = "1";
            pbc.NextAppStepEmpNo = hf_empno.Value.Trim();
        }

        else
        {
            if (hf_SuperiorId.Value == "")
                setSuperior();

            isFinish = "0";
            pbc.NextAppStepEmpNo = hf_SuperiorId.Value;

            if (isApprove == false)
                pbc.NextAppStepEmpNo = hf_proposerNo.Value;
        }

        pbc.CurrentAppStep = hfAppStep.Value.Trim();
        pbc.LastMenuID = hf_menuID.Value.Trim();
        pbc.PicCurrentApproval = hf_empno.Value.Trim();
        pbc.LastTaskID = task;
        pbc.NextAppStep = nextStep;
        pbc.HeadID = HF_OTDTID.Value;
        pbc.IsApprove = approve;
        pbc.IsFinish = isFinish;
        pbc.NextAppStepAppTaskID = task;
        pbc.Comment = hfAppTaskStep.Value.Trim();
        pbc.NextAppStepMenuID = hf_menuID.Value.Trim();

        if (nextStep == "1000" && hfAppTaskStep.Value == "1")
        {
            //hfAppTaskStep.Value = "2";
            tasknext = getOTTaskid("2");
            pbc.NextAppStepAppTaskID = tasknext;
        }

        hffriskainfo.Value = "appstep" + hfAppStep.Value.Trim() + "/id" + HF_OTDTID.Value + "/nextstep"
           + nextStep + "/next EmpNo" + hf_SuperiorId.Value + "/taskStep" + hfAppTaskStep.Value.Trim();


        return pbc;
    }
    private bool GetfingerPrint(string assetEmpNo)
    {
        string msgError = string.Empty;
        bool VerifySucceed = false;
        string ipAddress = hf_ipaddress.Value.Trim();
        string result = string.Empty;

        try
        {
            //blm support
            // VerifySucceed = pgs.FingerCheck(ipAddress, assetEmpNo.Trim(), out msgError);
            VerifySucceed = true;
        }
        catch
        {
            msgError = string.Format("alert('Service is not available or not started');");
            ScriptManager.RegisterStartupScript(UPUtama, this.GetType(), "click", msgError, true);
        }


        if (VerifySucceed)
        {
            msgError = string.Format("alert('Successed');");
            ScriptManager.RegisterStartupScript(UPUtama, this.GetType(), "click", msgError, true);
        }
        else
        {
            if (string.IsNullOrEmpty(result))
            {
                result = "Verification Failed";
            }
            msgError = string.Format("alert('{0}');", result);
            ScriptManager.RegisterStartupScript(UPUtama, this.GetType(), "click", msgError, true);
        }
        return VerifySucceed;
    }

    #endregion

    #region mailApprove
    private void mailApprove()
    {
        string finish = "0";

        string OTDTID = string.Empty;
        string Division = string.Empty;
        string Departement = string.Empty;
        //string Section = string.Empty;
        //string Line = string.Empty;
        string ProposedBy = string.Empty;
        string ShiftCode = string.Empty;
        string Date = string.Empty;
        string StartTime = string.Empty;
        string EndTime = string.Empty;
        string Duration = string.Empty;
        string Task = string.Empty;
        string dtEmployee = string.Empty;
        string status = string.Empty;
        string finishStatus = string.Empty;
        string complate = string.Empty;
        string proposed = string.Empty;
        string employee_id = string.Empty;
        string taskstep = string.Empty;
        string normalform = string.Empty;
        string steptittle = string.Empty;
        string taskid = string.Empty;
        string holy = string.Empty;
        string AppStepEmpName = string.Empty;

        OTDTID = HF_OTDTID.Value.Trim();
        Division = lblDivision.Text.Trim();
        Departement = hf_deptemail.Value.ToString();
        //Section = ddlSection.SelectedValue.Trim();
        //Line = ddlLine.SelectedValue.Trim();
        ProposedBy = lblProposedBy.Text.Trim();
        ShiftCode = ddlShiftCode.SelectedValue.Trim();
        Date = txtDate.Text.Trim();
        StartTime = txtstarttime.Text.Trim().Replace(".",":");
        EndTime = txtEndTime.Text.Trim().Replace(".", ":");
        Duration = lblDuration.Text.Trim();
        Task = txtTask.Text.Trim();
        DataTable dt = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", OTDTID, "", "=", "", "", "", "", ""));
        dtEmployee = ime.GetEmployeeName(dt.Rows[0]["AppStepEmpno"].ToString());
        status = dt.Rows[0]["LastStatus"].ToString();
        proposed = ime.GetEmployeeName(dt.Rows[0]["insertby"].ToString());
        employee_id = dt.Rows[0]["insertby"].ToString();
        taskstep = dt.Rows[0]["appTaskStep"].ToString();
        normalform = dt.Rows[0]["normal_name"].ToString();
        steptittle = dt.Rows[0]["AppStepTitle"].ToString();
        finishStatus = dt.Rows[0]["appStep"].ToString();
        taskid = dt.Rows[0]["AppTaskID"].ToString();
        holy = dt.Rows[0]["holiday_bool"].ToString();
        AppStepEmpName = dt.Rows[0]["AppStepEmpName"].ToString();

        string info = "ID= " + OTDTID + " /AppStep= " + finishStatus + " /TaskStep= " + taskstep + "/Holiday(1)Daily(0)= " + holy +
                           " /AppKe= " + AppStepEmpName + " /InsertBy = " + proposed;

        if (finishStatus == "1000" && status == "OK")
        {
            complate = "COMPLETE";
            hf_SuperiorId.Value = employee_id;
        }
        else if (finishStatus == "0" && taskstep == "2")
        {
            complate = "ONPROGRESS";
        }
        else
        {
            complate = "ONPROGRESS";
        }

        if (status == "NG")
            complate = "REJECTED";


        DataTable dtMail = new DataTable();
        //dtMail = inst_header.GetEmailOverTime_dtHeader(HF_OTDTID.Value.ToString());
        dtMail = this.pmt.createMailBodyTable();
        //string[] paramMailCC = { "", "", "" };
        dtMail.Rows.Add("Proposed By", ProposedBy);
        dtMail.Rows.Add("Divisi", Division);
        dtMail.Rows.Add("Departement", Departement);
        dtMail.Rows.Add("Shift Code", ShiftCode);
        dtMail.Rows.Add("Form", normalform);
        dtMail.Rows.Add("Date", Date);
        if (normalform == "normal")
        {
            dtMail.Rows.Add("Start Time", StartTime);
            dtMail.Rows.Add("End Time", EndTime);
            dtMail.Rows.Add("Duration", Duration);
        }
        dtMail.Rows.Add("Task", Task);
        dtMail.Rows.Add("Step Title", steptittle);
        dtMail.Rows.Add("Status", complate);

        DataTable dtUserEmail = new DataTable();
        DataTable dtUserName = new DataTable();
        //string superiorId = this.pgc.getDropdownValue(DdSuperior);
        string[] listspv = hf_SuperiorId.Value.Split(',');
        for (int i = 0; i < listspv.Count(); i++)
        {
            string superiorId = listspv[i];

            if (complate == "REJECTED")
            {
                superiorId = hf_proposerNo.Value;
            }
            string mailTo = string.Empty;
            string mailToName = string.Empty;
            string from = string.Empty;
            string fromName = string.Empty;

            if (string.IsNullOrEmpty(superiorId))
            {
                dtUserEmail = Inst_apouser.apoEmail(hf_empno.Value);
                dtUserName = Inst_apouser.apoEmail(employee_id);
                mailTo = dtUserEmail.Rows[0]["Email"].ToString();
                mailToName = dtUserEmail.Rows[0]["FullName"].ToString();
                from = dtUserName.Rows[0]["Email"].ToString();
                fromName = dtUserName.Rows[0]["FullName"].ToString();
            }
            else
            {
                dtUserEmail = Inst_apouser.apoEmail(superiorId);
                dtUserName = Inst_apouser.apoEmail(hf_empno.Value);
                if (!string.IsNullOrEmpty(dtUserEmail.Rows[0]["email"].ToString().Trim()))
                {
                    mailTo = dtUserEmail.Rows[0]["email"].ToString();
                }

                //mailTo = dtUserEmail.Rows[0]["Email"].ToString();
                mailToName = dtUserEmail.Rows[0]["FullName"].ToString();
                from = dtUserName.Rows[0]["Email"].ToString();
                fromName = dtUserName.Rows[0]["FullName"].ToString();
            }
            //subject email
            string subject = string.Empty;
            string mailBody = string.Empty;
            string mailBodyFriska = string.Empty;
            string SubjectAdmin = string.Empty;
            string refno = "Refno " + hf_menuID.Value + "-";

            if (finishStatus == null)
                finishStatus = "0";

            if (status.Trim().Replace(" ", "") == "OK")
            {
                if (finishStatus.Trim().Replace(" ", "") == "1000") //complete
                {
                    mailTo = from;
                    subject = this.pmt.createSubject(this.pmt.SubjectComplete, applicationName, subjectApp + refno + OTDTID, "Been Approved", mailToName);
                    mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
                        this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);

                    mailBodyFriska = this.pmt.createMessage(classname + hffriskainfo.Value, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
                        this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);


                }
                else
                {
                    subject = this.pmt.createSubject(this.pmt.SubjectComplete, applicationName, subjectApp + refno + OTDTID, "Submitted", fromName);
                    mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.OpeningNeedApproval,
                    this.pmt.BodyTypeList, mailToName, "1", hf_menuID.Value.Trim(), dtMail, subjectApp);

                    mailBodyFriska = this.pmt.createMessage(classname + hffriskainfo.Value, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
                      this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);
                }
            }
            else if (status.Trim().Replace(" ", "") == "NG")
            {
                mailTo = from;
                subject = "[" + applicationName + "] " + subjectApp + refno + OTDTID + " is Rejected";
                mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
                            this.pmt.BodyTypeList, mailToName, "1", hf_menuID.Value.Trim(), dtMail, subjectApp);
            }


            //this.pgsm.SendByWS(subject, mailBody, "friskadwina.sari@sep.epson.com.sg");
            //this.pgsm.SendByWS(info, mailBodyFriska, "friskadwina.sari@sep.epson.com.sg");--
            //this.pgsm.SendByWS(subject, mailBody, "aprida.sari@sep.epson.com.sg");
            //====================================================================
            //this.pgsm.SendMail(subject, mailBody, true, mailTo, paramMailCC);
            if (mailTo != string.Empty)
            {
                this.pgsm.SendMail(subject, mailBody, true, mailTo);
            }
            //this.pgsm.SendMail(subject, mailBody, true, mailTo);
            //this.pgsm.SendMail(info, mailBodyFriska, true, "friskadwina.sari@sep.epson.com.sg", "");

        }

    }

    private void mailTest()
    {
        //string finish = "0";

        //string OTDTID = string.Empty;
        //string Division = string.Empty;
        //string Departement = string.Empty;
        ////string Section = string.Empty;
        ////string Line = string.Empty;
       // string ProposedBy = string.Empty;
        //string ShiftCode = string.Empty;
        //string Date = string.Empty;
        //string StartTime = string.Empty;
        //string EndTime = string.Empty;
        //string Duration = string.Empty;
        //string Task = string.Empty;
        //string dtEmployee = string.Empty;
        //string status = string.Empty;
        //string finishStatus = string.Empty;
        //string complate = string.Empty;
        string proposed = string.Empty;
        //string employee_id = string.Empty;
        //string taskstep = string.Empty;
        //string normalform = string.Empty;
        //string steptittle = string.Empty;
        //string taskid = string.Empty;
        //string holy = string.Empty;
        //string AppStepEmpName = string.Empty;

        //OTDTID = HF_OTDTID.Value.Trim();
        //Division = lblDivision.Text.Trim();
        //Departement = hf_deptemail.Value.ToString();
        ////Section = ddlSection.SelectedValue.Trim();
        ////Line = ddlLine.SelectedValue.Trim();
       // ProposedBy = lblProposedBy.Text.Trim();
        //ShiftCode = ddlShiftCode.SelectedValue.Trim();
        //Date = txtDate.Text.Trim();
        //StartTime = txtstarttime.Text.Trim();
        //EndTime = txtEndTime.Text.Trim();
        //Duration = lblDuration.Text.Trim();
        //Task = txtTask.Text.Trim();
        //DataTable dt = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", OTDTID, "", "=", "", "", "", "", ""));
        //dtEmployee = ime.GetEmployeeName(dt.Rows[0]["AppStepEmpno"].ToString());
        //status = dt.Rows[0]["LastStatus"].ToString();
        proposed = "Indriani Dewi Safitri";
        //employee_id = dt.Rows[0]["insertby"].ToString();
        //taskstep = dt.Rows[0]["appTaskStep"].ToString();
        //normalform = dt.Rows[0]["normal_name"].ToString();
        //steptittle = dt.Rows[0]["AppStepTitle"].ToString();
        //finishStatus = dt.Rows[0]["appStep"].ToString();
        //taskid = dt.Rows[0]["AppTaskID"].ToString();
        //holy = dt.Rows[0]["holiday_bool"].ToString();
        //AppStepEmpName = dt.Rows[0]["AppStepEmpName"].ToString();

        //string info = "ID= " + OTDTID + " /AppStep= " + finishStatus + " /TaskStep= " + taskstep + "/Holiday(1)Daily(0)= " + holy +
        //                   " /AppKe= " + AppStepEmpName + " /InsertBy = " + proposed;

        //if (finishStatus == "1000" && status == "OK")
        //{
        //    complate = "COMPLETE";
        //}
        //else if (finishStatus == "0" && taskstep == "2")
        //{
        //    complate = "ONPROGRESS";
        //    hf_SuperiorId.Value = employee_id;
        //}
        //else
        //{
        //    complate = "ONPROGRESS";
        //}

        //if (status == "NG")
        //    complate = "REJECTED";


        DataTable dtMail = new DataTable();
        //dtMail = inst_header.GetEmailOverTime_dtHeader(HF_OTDTID.Value.ToString());
        dtMail = this.pmt.createMailBodyTable();
        string[] paramMailCC = { "indriani.dewi@delogi.co.id", "", "" };
        dtMail.Rows.Add("Proposed By indri");
        dtMail.Rows.Add("Divisi test");
        //dtMail.Rows.Add("Departement", Departement);
        //dtMail.Rows.Add("Shift Code", ShiftCode);
        //dtMail.Rows.Add("Form", normalform);
        //dtMail.Rows.Add("Date", Date);
        //if (normalform == "normal")
        //{
        //    dtMail.Rows.Add("Start Time", StartTime);
        //    dtMail.Rows.Add("End Time", EndTime);
        //    dtMail.Rows.Add("Duration", Duration);
        //}
        //dtMail.Rows.Add("Task", Task);
        //dtMail.Rows.Add("Step Title", steptittle);
        //dtMail.Rows.Add("Status", complate);

        //DataTable dtUserEmail = new DataTable();
        //DataTable dtUserName = new DataTable();
        ////string superiorId = this.pgc.getDropdownValue(DdSuperior);
        //string superiorId = hf_SuperiorId.Value;
        //if (complate == "REJECTED")
        //{
        //    superiorId = hf_proposerNo.Value;
        //}
        string mailTo = string.Empty;
        string mailToName = string.Empty;
        string from = string.Empty;
        string fromName = string.Empty;


        mailTo = "Indriani.dewi@delogi.co.id";
        mailToName = "Indriani Dewi Safitri";
        from = "indrianids94@gmail.com";
        fromName = "HCM System";

        string subject = string.Empty;
        string mailBody = string.Empty;
        string mailBodyFriska = string.Empty;
        string SubjectAdmin = string.Empty;

        mailTo = from;
        subject = this.pmt.createSubject(this.pmt.SubjectComplete, applicationName, subjectApp, "Been Approved", mailToName);

        mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
            this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);

        mailBodyFriska = this.pmt.createMessage(classname + hffriskainfo.Value, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
            this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);


        //if (string.IsNullOrEmpty(superiorId))
        //{
        //    dtUserEmail = Inst_apouser.apoEmail(hf_empno.Value);
        //    dtUserName = Inst_apouser.apoEmail(employee_id);
        //    mailTo = dtUserEmail.Rows[0]["Email"].ToString();
        //    mailToName = dtUserEmail.Rows[0]["FullName"].ToString();
        //    from = dtUserName.Rows[0]["Email"].ToString();
        //    fromName = dtUserName.Rows[0]["FullName"].ToString();
        //}
        //else
        //{
        //    dtUserEmail = Inst_apouser.apoEmail(superiorId);
        //    dtUserName = Inst_apouser.apoEmail(hf_empno.Value);

        //    mailTo = dtUserEmail.Rows[0]["Email"].ToString();
        //    mailToName = dtUserEmail.Rows[0]["FullName"].ToString();
        //    from = dtUserName.Rows[0]["Email"].ToString();
        //    fromName = dtUserName.Rows[0]["FullName"].ToString();
        //}
        ////subject email
        //string subject = string.Empty;
        //string mailBody = string.Empty;
        //string mailBodyFriska = string.Empty;
        //string SubjectAdmin = string.Empty;
        //string refno = "Refno " + hf_menuID.Value + "-";

        //if (finishStatus == null)
        //    finishStatus = "0";

        //if (status.Trim().Replace(" ", "") == "OK")
        //{
        //    if (finishStatus.Trim().Replace(" ", "") == "1000") //complete
        //    {
        //        mailTo = from;
        //        subject = this.pmt.createSubject(this.pmt.SubjectComplete, applicationName, subjectApp + refno + OTDTID, "Been Approved", mailToName);
        //        mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
        //            this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);

        //        mailBodyFriska = this.pmt.createMessage(classname + hffriskainfo.Value, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
        //            this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);


        //    }
        //    else
        //    {
        //        subject = this.pmt.createSubject(this.pmt.SubjectComplete, applicationName, subjectApp + refno + OTDTID, "Submitted", fromName);
        //        mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.OpeningNeedApproval,
        //        this.pmt.BodyTypeList, mailToName, "1", hf_menuID.Value.Trim(), dtMail, subjectApp);

        //        mailBodyFriska = this.pmt.createMessage(classname + hffriskainfo.Value, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
        //          this.pmt.BodyTypeList, proposed, "1", hf_menuID.Value.Trim(), dtMail, applicationName);
        //    }
        //}
        //else if (status.Trim().Replace(" ", "") == "NG")
        //{
        //    mailTo = from;
        //    subject = "[" + applicationName + "] " + subjectApp + refno + OTDTID + " is Rejected";
        //    mailBody = this.pmt.createMessage(applicationName, this.pmt.HeaderByTitle, this.pmt.SubjectComplete,
        //                this.pmt.BodyTypeList, mailToName, "1", hf_menuID.Value.Trim(), dtMail, subjectApp);
        //}


        //this.pgsm.SendByWS(subject, mailBody, "friskadwina.sari@sep.epson.com.sg");
        //this.pgsm.SendByWS(info, mailBodyFriska, "friskadwina.sari@sep.epson.com.sg");--
        //this.pgsm.SendByWS(subject, mailBody, "aprida.sari@sep.epson.com.sg");
        //====================================================================
        this.pgsm.SendMail(subject, mailBody, true, mailTo, paramMailCC);
        //this.pgsm.SendMail(info, mailBodyFriska, true, "friskadwina.sari@sep.epson.com.sg", "");

    }
    #endregion

    #region SaveUpdate
    private bool SaveUpdateHeader(bool IsSave)
    {
        bool bola = false;
        mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
        kls01 = new OverTime_dtHeader();
        kls01.DivisiID = lbldivID.Text;
        //kls01.DeptID = pgc.getDropdownValue(ddlDept);
        kls01.DeptID = lblDepartment.Text;
        kls01.LineID = pgc.getDropdownValue(ddlLine);
        kls01.Normal_bool = HFOTTypeForm.Value;
        kls01.OTTaskDesc = pgc.safeString(txtTask.Text);
        kls01.Pic = hf_empno.Value;
        kls01.RequestDate = pgc.safeString(txtDate.Text);
        kls01.SectionID = pgc.getDropdownValue(ddlSection);
        kls01.ShiftCode = pgc.getDropdownValue(ddlShiftCode);
        kls01.OTdtID = HF_OTDTID.Value;
        if (IsSave)
        {
            HF_OTDTID.Value = mgr01.Save(kls01).ToString();
            if (pgc.safeInt(HF_OTDTID.Value) > 0)
                bola = true;
        }
        else
            bola = mgr01.Update(kls01);

        return bola;
    }
    private bool SaveUpdateDetail(bool IsSave, bool IsChecked, string cekupdate, GridView gv)
    {
        bool bola = true;
        mgr02 = InstAttendanceOTDT.GetInstanceOverTime_dtDetail;
        List<OverTime_dtDetail> item = new List<OverTime_dtDetail>();

        DataTable dtTemp = new DataTable();
        if (gv == GV01)
        {
            dtTemp = GridviewChecked(gv, IsChecked, false);
        }
        else
        {
            dtTemp = GridviewChecked(gv, IsChecked, false);
        }
        if (dtTemp.Rows.Count > 0)
        {

            for (int i = 0; i < dtTemp.Rows.Count; i++)
            {
                kls02 = new OverTime_dtDetail();
                kls02.AdjustmentDuration = dtTemp.Rows[i]["txtAdjDuration"].ToString();
                kls02.AdjustmentRemark = dtTemp.Rows[i]["txtAdjRemark"].ToString();
                kls02.EmployeeNo = dtTemp.Rows[i]["lblEmpNo"].ToString();
                kls02.OTDetailsID = dtTemp.Rows[i]["lblDetailsID"].ToString();
                kls02.PlanDuration = dtTemp.Rows[i]["lblPDuration"].ToString();
                kls02.OTdtID = HF_OTDTID.Value;
                kls02.Pic = hf_empno.Value;
                kls02.IsAgree = dtTemp.Rows[i]["hfIsAgree"].ToString();
                string timeFrom = txtDate.Text.Trim() + " " + dtTemp.Rows[i]["txtPTimeFrom"].ToString().Replace(".",":");
                string timeTo = txtDate.Text.Trim() + " " + dtTemp.Rows[i]["txtPTimeTo"].ToString().Replace(".", ":");
                //kls02.PlanTimeFrom = pgc.getDateFrom(timeFrom, timeTo);
                kls02.PlanTimeFrom = timeFrom;
                //kls02.PlanTimeTo = pgc.getDateTO(timeFrom, timeTo);
                kls02.PlanTimeTo = timeTo;

                bool benar = pgc.checkDateTime(Convert.ToDateTime(timeFrom), Convert.ToDateTime(timeTo));
                if (benar)
                {
                    DateTime x = Convert.ToDateTime(txtDate.Text).AddDays(1);
                    timeTo = x.ToString("yyyy-MM-dd") + " " + dtTemp.Rows[i]["txtPTimeTo"].ToString();
                }

                string actualtimeFrom = txtDate.Text.Trim() + " " + dtTemp.Rows[i]["lblActualTimeFrom"].ToString().Replace(".", ":");
                string actualtimeTo = txtDate.Text.Trim() + " " + dtTemp.Rows[i]["lblActualTimeTo"].ToString().Replace(".", ":");
                kls02.ActualTimeFrom = actualtimeFrom;
                kls02.ActualTimeTo = actualtimeTo;
                kls02.ActualDuration = dtTemp.Rows[i]["lblActualDuration"].ToString();

                string actualduration = dtTemp.Rows[i]["lblActualDuration"].ToString();
                if (string.IsNullOrEmpty(actualduration))
                {
                    //actualduration = actualduration.Replace(",", ".").Trim();
                    kls02.ActualDuration = kls02.AdjustmentDuration;
                }
                item.Add(kls02);
            }
        }

        if (cekupdate == "Save")
        {
            if (HFOTTypeForm.Value.ToLower() == "0")
            {
                mgr02.SaveAbNormal(item, hf_empno.Value);
            }
            else
                mgr02.SaveNormal(item, hf_empno.Value);
        }
        else if (!string.IsNullOrEmpty(kls02.AdjustmentDuration))
        {
            mgr02.UpdateAdjustment(item, hf_empno.Value);
        }
        else
        {
            mgr02.UpdateBefore(item, hf_empno.Value);
        }
        return bola;
    }
    #endregion

    #region DisEnaBle
    private void disable()
    {
        lblrefID.Enabled = false;
        lblDivision.Enabled = false;
        lblDepartment.Enabled = false;
        ddlDept.Enabled = false;
        ddlSection.Enabled = false;
        ddlLine.Enabled = false;
        lblProposedBy.Enabled = false;
        ddlShiftCode.Enabled = false;
        txtDate.Enabled = false;
        lbldeptcadangan.Enabled = false;
    }
    private void enable()
    {
        lbldeptcadangan.Enabled = true;
        lblrefID.Enabled = true;
        lblDivision.Enabled = true;
        lblDepartment.Enabled = true;
        ddlDept.Enabled = true;
        ddlSection.Enabled = true;
        ddlLine.Enabled = true;
        txtTask.Enabled = true;
        lblProposedBy.Enabled = true;
        ddlShiftCode.Enabled = true;
        txtDate.Enabled = true;
        txtstarttime.Enabled = true;
        txtEndTime.Enabled = true;
        lblDuration.Enabled = true;
    }

    private void visibleToAbnormal()
    {
        txtstarttime.Visible = false;
        txtEndTime.Visible = false;
        lbl.Visible = false;
        lbltitik.Visible = false;
        lblto.Visible = false;
        pgc.GVColumnVisibility(GV01, new[] { "Actual T.From", "Actual T.To", "Act Drtn", "Adj Drtn", "Remark", "PDW", "Real Duration" }, new[] { "IsAgree", "IsProposed" });

    }
    #endregion

    #region Button
    protected void Btn_Command(object sender, EventArgs e)
    {
        //pgm = new PapuaUtilities.GeneralMessage.PapuaGeneralMessage();
        DataTable dtTemp = new DataTable();

        if (((Button)sender).CommandName == "Simpan")
        {


            dtTemp = this.GridviewChecked(GV01, true, false);

            //cek data dalam periode
            if (lblHoliday.Text == "Date is Not valid")
            {
                WebHelper.MessageBoxAjaxs(UPUtama,  "Date & Time Must Valid in Periode", false);
            }
            else
            {
                if (pgc.setAccessSave(ddlSection, ddlShiftCode, txtDate, txtTask) & dtTemp.Rows.Count > 0)
                {
                    bool bola = false;
                    if (this.SaveUpdateHeader(true))
                        bola = this.SaveUpdateDetail(true, true, "Save", GV01);
                    if (bola)
                    {
                        WebHelper.MessageBoxAjaxs(UPUtama, "Data has been saved!", false);
                        btnSave.Visible = false;
                        disable();
                        lblrefID.Text = hf_menuID.Value + "-" + HF_OTDTID.Value;
                        btnUpdate.Visible = true;
                        btnSearch0.Visible = false;
                        

                        lblnextApp.Visible = true;
                        lblnextApp.Text = "Next Approval";
                        lbltitikApp.Visible = true;
                        setSuperior();

                        PapuaBLCommonParamExt pcpe = new PapuaBLCommonParamExt();
                        pcpe.ParamField1 = "A.OTdtID";
                        pcpe.ParamFieldData1 = HF_OTDTID.Value.ToString();
                        pcpe.Operrator1 = "=";
                        DataTable dtdet = inst_detail.GetOverTime_dtDetail(pcpe);

                        string isAgree = string.Empty;
                        string isProposed = string.Empty;
                        string proposeId = string.Empty;

                        for (int i = 0; i < dtdet.Rows.Count; i++)
                        {
                            proposeId = dtdet.Rows[0]["ProposeId"].ToString();
                            isAgree = dtdet.Rows[0]["isAgree"].ToString();
                            isProposed = dtdet.Rows[0]["IsProposed"].ToString();

                            if (isAgree == "0" || isProposed == "0")
                            {
                                sembunyiHF(GV01, isProposed, isAgree);
                            }

                        }
                        BindDataGV(GV01, dtdet);

                        panggilHilangkanField();


                    }
                    else
                    {
                        WebHelper.MessageBoxAjaxs(UPUtama, "Data has not been saved !", false);
                    }

                }
                else
                {
                   // WebHelper.MessageBoxAjaxs(UPUtama, new[] { "Data has not been saved !", "You must fill these fields :", "Section", "Shift Cide", "Date & Time must valid in periode", "Task", "Minimum 1 employee checked" }, false);
                    WebHelper.MessageBoxAjaxs(UPUtama, "Data has not been saved !", false);
                }

            }

        }

        else if (((Button)sender).CommandName == "Update")
        {
            dtTemp = this.GridviewChecked(GV01, true, false);
            if (pgc.setAccessSave(ddlSection, ddlShiftCode, txtDate) & dtTemp.Rows.Count > 0)
            {
                bool bola = false;
                if (this.SaveUpdateHeader(false))
                {
                    bola = this.SaveUpdateDetail(true, true, "Update", GV01);


                }

                if (bola)
                {
                    WebHelper.MessageBoxAjaxs(UPUtama, "Data has been Updated!", false);
                }
                else
                {
                    WebHelper.MessageBoxAjaxs(UPUtama, "Data has not been updated!", false);
                }
            }
            else
            {
//                WebHelper.MessageBoxAjaxs(UPUtama, new[] { "Data has not been updated !", "You must fill these fields :", "Section", "Shift Cide", "Date & Time", "Minimum 1 employee checked" }, false);
                  WebHelper.MessageBoxAjaxs(UPUtama,  "Data has not been updated !", false);

            }

        }

        else if (((Button)sender).CommandName == "Submit")
        {
            dtTemp = this.GridviewChecked(GV01, true, true);
            if (pgc.setAccessSave(ddlSection, ddlShiftCode, txtDate, hf_SuperiorId) & dtTemp.Rows.Count > 0)
            {
                if (CountingError() == 0)
                {
                    if (ApproveReject(true))
                    {
                        dtTemp = IsAgree(GV01);
                        checkRejectOTEmployee(dtTemp);
                        mailApprove();

                        TabOtApplyMain = "1";
                        this.ResponseRedirectPage();

                    }
                    else
                    {
                       WebHelper.MessageBoxAjaxs(UPUtama, "Sorry, Data has not been submitted!", false);

                        TabOtApplyMain = "";
                        this.ResponseRedirectPage();

                    }
                }
                else
                {
                    WebHelper.MessageBoxAjaxs(UPUtama, "Sorry, Data has not been submitted!", false);

                }

            }
            else
            {
               // WebHelper.MessageBoxAjaxs(UPUtama,  "Data has not been submitted !", "You must fill these fields :", "Next Approval", "Finger Print", "Date", "Time To", "Time From", "Checked" }, false);
            WebHelper.MessageBoxAjaxs(UPUtama, "Data has not been submitted !" , false);
                 }

            }

        else if (((Button)sender).CommandName == "Approve")
        {
            dtTemp = this.GridviewChecked(GV01, false, true);
            if (pgc.setAccessSave(ddlSection, ddlShiftCode, txtDate)) // & dtTemp.Rows.Count > 0
            {
                string error = string.Empty;
                string finger = ica.GetIsFingerPrint(out error , hf_menuID.Value, hf_taskID.Value, hfAppStep.Value);
                bool result = false;
                finger = "1";
                if (finger == "1") // using  finger Print
                {
                    if (hfFingerPrint.Value == "1")
                        result = GetfingerPrint(hf_empno.Value);
                    //result = true;
                    else
                        result = false;
                    if (result)
                    {
                        if (ApproveReject(true))
                        {
                            mailApprove();
                            WebHelper.MessageBoxAjaxs(UPUtama, "Data has been approved", false);

                            TabOtApplyMain = "2";
                            this.ResponseRedirectPage();
                        }
                        else
                        {


                            WebHelper.MessageBoxAjaxs(UPUtama,"Sorry, Data has not been approved!", false);
                            TabOtApplyMain = "";
                            this.ResponseRedirectPage();

                        }
                    }
                    else
                    {
                        WebHelper.MessageBoxAjaxs(UPUtama, "Sorry, Data has not been approved!", false);
                    }
                }
                else //tanpa finger
                {
                    string date = txtDate.Text.ToString();
                    string time = DateTime.Now.ToString("yyyy-MM-dd");

                    if ((hfAppTaskStep.Value == "1"))
                    {
                        if (ApproveReject(true))
                        {
                            mailApprove();
                            WebHelper.MessageBoxAjaxs(UPUtama, "Data has been approved", false);
                            TabOtApplyMain = "2";
                            this.ResponseRedirectPage();
                        }
                        else
                        {
                            WebHelper.MessageBoxAjaxs(UPUtama, "Data has been approved", false);
                            TabOtApplyMain = "";
                            this.ResponseRedirectPage();
                        }
                    }
                    else if (hfAppTaskStep.Value == "2" && (Convert.ToDateTime(date) < Convert.ToDateTime(time)))
                    {
                        if (ApproveReject(true))
                        {
                            mailApprove();
                            WebHelper.MessageBoxAjaxs(UPUtama, "Data has been approved", false);

                            TabOtApplyMain = "2";
                            this.ResponseRedirectPage();
                        }
                        else
                        {
                            WebHelper.MessageBoxAjaxs(UPUtama,"Sorry, Data has not been approved", false);
                            TabOtApplyMain = "";
                            this.ResponseRedirectPage();
                        }
                    }
                    else
                    {
                        WebHelper.MessageBoxAjaxs(UPUtama, "Actual is Empty", false);
                    }


                }


            }
        }
        else if (((Button)sender).CommandName == "Reject")
        {
            string error = string.Empty;
            string finger = ica.GetIsFingerPrint(out error, hf_menuID.Value, hf_taskID.Value, hfAppStep.Value);
            bool result = false;
            if (finger == "1") // using  finger Print
            {
                if (hfFingerPrint.Value == "1")
                    result = GetfingerPrint(hf_empno.Value);
                //result = true;
                else
                    result = false;
                if (result)
                {
                    if (ApproveReject(false))
                    {
                        //mailApprove();
                        WebHelper.MessageBoxAjaxs(UPUtama, "Data has been rejected", false);
                        TabOtApplyMain = "3";
                        this.ResponseRedirectPage();
                    }
                    else
                    {
                        WebHelper.MessageBoxAjaxs(UPUtama, "Data has not been rejected", false);
                        TabOtApplyMain = "";
                        this.ResponseRedirectPage();
                    }
                }
                else
                {
                    WebHelper.MessageBoxAjaxs(UPUtama,"Data has not been rejected", false);
                }

            }
            else
            {
                ApproveReject(false);
                //mailApprove();
                WebHelper.MessageBoxAjaxs(UPUtama, "Data has been rejected",false);

                DataTable dttemp = new DataTable();
                dttemp = inst_header.GetOverTime_dtHeader(setPcpa("A.OTdtID", "", HF_OTDTID.Value.ToString(), "", "=", "", "", "", "", ""));
                string isNulla = string.Empty;
                for (int i = 0; i < dttemp.Rows.Count; i++)
                {
                    isNulla = dttemp.Rows[i]["lastStatus"].ToString();
                    if (isNulla == "NG")
                    {

                        TabOtApplyMain = "3";
                        this.ResponseRedirectPage();
                    }
                    else
                    {
                        TabOtApplyMain = "";
                        this.ResponseRedirectPage();
                    }
                }
            }

        }
        else if (((Button)sender).CommandName == "UpdateSimpan")
        {


            if (pgc.setAccessSave(ddlSection, ddlShiftCode, txtDate, txtEndTime, txtstarttime))
            {
                dtTemp = this.GridviewChecked(GV01, false, false);
            }
        }
        else if (((Button)sender).CommandName == "Search")
        {
            mgr01 = InstAttendanceOTDT.GetInstanceOverTime_dtHeader;
            string section = string.Empty;
            string line = string.Empty;
            string PlanTimeFrom = string.Empty;
            string PlanTimeTo = string.Empty;
            string shift = string.Empty;
            string date = string.Empty;
            string actualTimeFrom = string.Empty;
            string actualTimeTo = string.Empty;
            string test = string.Empty;
            string empno = string.Empty;
            string empname = string.Empty;
             
            dtTemp = mgr01.GetOverTimeEmployee(lbldivID.Text, lblDepartment.Text, section, line, PlanTimeFrom, PlanTimeTo, shift, actualTimeFrom, actualTimeTo, empno, empname, lblOTForm.Text);
            if (dtTemp.Rows.Count > 0)
            {
                //BindDataGV(GV02, dtTemp);
            }
            else
            {
                WebHelper.MessageBoxAjaxs(UPUtama, "Application Already Exist !", false);
            }


        }
        else if (((Button)sender).CommandName == "BuangEmp")
        {
            List<PapuaIdentityID> identity = new List<PapuaIdentityID>();
            int rowsCount = GV01.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                CheckBox cb = (CheckBox)GV01.Rows[i].FindControl("chkCheck");
                Label lblDetailsID = (Label)GV01.Rows[i].FindControl("lblDetailsID");

                if (cb.Checked)
                {
                    PapuaIdentityID piD = new PapuaIdentityID();
                    piD.ID = lblDetailsID.Text.ToString();
                    identity.Add(piD);
                }
            }
            bool s = inst_detail.DeleteByOverTime_dtDetailID(hf_empno.Value.Trim(), identity);
            if (s == true)
            {
                WebHelper.MessageBoxAjaxs(UPUtama, "Data has been Deleted", false);
                GV01.EditIndex = -1;
                SetOTEmployee(GV01);

                PapuaBLCommonParamExt pcpe = new PapuaBLCommonParamExt();
                pcpe.ParamField1 = "A.OTdtID";
                pcpe.ParamFieldData1 = HF_OTDTID.Value.ToString();
                pcpe.Operrator1 = "=";
                DataTable dtdet = inst_detail.GetOverTime_dtDetail(pcpe);

                string isAgree = string.Empty;
                string isProposed = string.Empty;
                string proposeId = string.Empty;

                for (int i = 0; i < dtdet.Rows.Count; i++)
                {
                    proposeId = dtdet.Rows[0]["ProposeId"].ToString();
                    isAgree = dtdet.Rows[0]["isAgree"].ToString();
                    isProposed = dtdet.Rows[0]["IsProposed"].ToString();

                    if (isAgree == "0" || isProposed == "0")
                    {
                        sembunyiHF(GV01, isProposed, isAgree);
                    }

                }
                BindDataGV(GV01, dtdet);

              //  SetOTEmployeeTanpaParam(GV02);
            }
            else
            {
                WebHelper.MessageBoxAjaxs(UPUtama, pgm.msgDeletedFail(), false);
            }

        }

        
         
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        //SetSession();
        if (hf_modeView.Value.ToString() == tbl01)
        {
            TabOtApplyMain = "0";
        }
        else if (hf_modeView.Value.ToString() == tbl02)
        {
            TabOtApplyMain = "1";
        }
        else if (hf_modeView.Value.ToString() == tbl03)
        {
            TabOtApplyMain = "2";
        }
        else if (hf_modeView.Value.ToString() == tbl04)
        {
            TabOtApplyMain = "3";
        }
        else if (hf_modeView.Value.ToString() == tbl05)
        {
            TabOtApplyMain = "4";
        }
        else if (hf_modeView.Value.ToString() == tbl06)
        {
            TabOtApplyMain = "5";
        }
        else
        {
            TabOtApplyMain = "0";
        }

        ResponseRedirectPage();
    }

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        int totalRow = GV01.Rows.Count;
        CheckBox cbCheckListAll = (CheckBox)GV01.HeaderRow.FindControl("chkAll");
        for (int i = 0; i < totalRow; i++)
        {
            CheckBox cbCheckList = (CheckBox)GV01.Rows[i].FindControl("chkCheck");
            if (cbCheckListAll.Checked && cbCheckList.Enabled == true)
            {
                cbCheckList.Checked = true;
            }
            else
            {
                cbCheckList.Checked = false;
            }
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //cara check finger ini
        //CheckFinger();
        bool result = false;
        if (hfFingerPrint.Value == "1")
            result = GetfingerPrint(hf_empno.Value);
        else
            result = true;
        if (result)
        {
            //approve & reject
        }
    }

    protected void btnSearch0_Click(object sender, EventArgs e)
    {

        SetOTEmployee(GV01);
        panelList.Visible = true;
    }
    #endregion

    private PapuaBLCommonParamWithApprovalExt setPcpa(string paramField1, string paramField2, string paramFieldData1, string paramFieldData2, string operatorr1, string operatorr2, string tab,
    string paramDateField, string paramDateFieldFrom, string paramDateFieldTo)
    {
        PapuaBLCommonParamWithApprovalExt pcp = new PapuaBLCommonParamWithApprovalExt();
        pcp.ParamField1 = paramField1;
        pcp.ParamField2 = paramField2;
        pcp.ParamFieldData1 = paramFieldData1;
        pcp.ParamFieldData2 = paramFieldData2;
        pcp.Operrator1 = operatorr1;
        pcp.Operrator2 = operatorr2;
        pcp.ParamTab = tab;
        pcp.ParamApprovalEmpNo = hf_empno.Value.Trim();
        pcp.ParamDateField = paramDateField;
        pcp.ParamDateFieldFrom = paramDateFieldFrom;
        pcp.ParamDateFieldTo = paramDateFieldTo;
        return pcp;
    }

    private int CountingError()
    {
        int er = 0;

        for (int i = 0; i < GV01.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)GV01.Rows[i].FindControl("chkCheck");
            HiddenField hf = (HiddenField)GV01.Rows[i].FindControl("hfIsAgree");

            if (cb.Checked && hf.Value != "1")
            {
                er += 1; //er=er+1
            }
        }
        return er;
    }

    private bool CheckFinger()
    {
        bool bola = false;
        string msgError = string.Empty;
        bool VerifySucceed = false;
        try
        {
            //blmsupport
         //   VerifySucceed = pgs.FingerCheck(pgc.GetIPAddress(), hf_empno.Value.Trim(), out msgError);
        }
        catch
        {
            bola = false;
            //string msgError = string.Format("alert('Service is not available or not started');");
            //ScriptManager.RegisterStartupScript(UP1, this.GetType(), "click", msgError, true);
        }
        return bola;
    }
    #region EXCELL
    protected void btnexcell_Click(object sender, EventArgs e)
    {
        DataTable dtTemp = inst_detail.GetOverTime_dtDetail(HF_OTDTID.Value);
        //ConvertToExcel(DataTempUbah(dtTemp), "Excell OverTime Detail", this.Page, ExcelField);
        pgc.ConvertToExcel(DataTempUbah(dtTemp), "Excell OverTime Detail", this.Page, ExcelField);
    }

    private DataTable DataTempUbah(DataTable dt)
    {
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            if (dt.Columns[i].ColumnName.ToString() == "employeeNo") dt.Columns[i].ColumnName = "Employee No";
            if (dt.Columns[i].ColumnName.ToString() == "employee_name") dt.Columns[i].ColumnName = "Employee Name";
            if (dt.Columns[i].ColumnName.ToString() == "planFromTime") dt.Columns[i].ColumnName = "From";
            if (dt.Columns[i].ColumnName.ToString() == "planToTime") dt.Columns[i].ColumnName = "To";
            if (dt.Columns[i].ColumnName.ToString() == "planDuration") dt.Columns[i].ColumnName = "Duration";
            if (dt.Columns[i].ColumnName.ToString() == "actualFromTime") dt.Columns[i].ColumnName = "Actual From";
            if (dt.Columns[i].ColumnName.ToString() == "actualToTime") dt.Columns[i].ColumnName = "Actual To";
            if (dt.Columns[i].ColumnName.ToString() == "actualDuration") dt.Columns[i].ColumnName = "Act Drtn";
            if (dt.Columns[i].ColumnName.ToString() == "adjustmentDuration") dt.Columns[i].ColumnName = "Adjustment";
            if (dt.Columns[i].ColumnName.ToString() == "adjustmentRemark") dt.Columns[i].ColumnName = "Remark";
            if (dt.Columns[i].ColumnName.ToString() == "resultDuration") dt.Columns[i].ColumnName = "Result Duration";
            if (dt.Columns[i].ColumnName.ToString() == "formulaDuration") dt.Columns[i].ColumnName = "Formula Duration";
            if (dt.Columns[i].ColumnName.ToString() == "PDW") dt.Columns[i].ColumnName = "PDW Duration";
        }
        return dt;
    }

    public void ConvertToExcel(DataTable dtTemp, string filename, Page p, string[] HeaderField)
    {
        try
        {
            if (HeaderField.Length > 0)
            {
                GridView gv = new GridView();
                gv.DataSource = setExcelTable(dtTemp, HeaderField);
                gv.AllowPaging = false;
                gv.AllowSorting = false;
                gv.HeaderStyle.ForeColor = System.Drawing.Color.Red;
                gv.DataBind();


                string attachment = string.Empty;
                attachment = "attachment; filename=" + filename + ".xls";
                if (string.IsNullOrEmpty(filename))
                {
                    attachment = "attachment; filename=data1.xls";
                }

                p.Response.ClearContent();

                p.Response.AddHeader("content-disposition", attachment);

                p.Response.ContentType = "application/vnd.ms-excel";

                StringWriter sw = new StringWriter();

                HtmlTextWriter htw = new HtmlTextWriter(sw);

                gv.RenderControl(htw);

                p.Response.Write(sw.ToString());

                p.Response.End();
            }

        }
        catch (Exception ex)
        { }
    }
    private DataTable setExcelTable(DataTable dtTemp, string[] columnField)
    {
        if (dtTemp.Rows.Count != 0)
        {
            for (int j = 0; j < columnField.Length; j++)
            {
                for (int i = 0; i < dtTemp.Columns.Count; i++)
                {
                    if (dtTemp.Columns[i].ColumnName == columnField[j].ToString())
                    {
                        dtTemp.Columns.Remove(dtTemp.Columns[i].ColumnName);
                    }
                }
            }
        }
        return dtTemp;
    }

    #endregion


     
}