﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventHandlingAttendanceDetail.aspx.cs" Inherits="PapuaWebForm.PEBPapuaUI.FirstLink_System.Training_Management.Transaction.EventHandlingAttendanceDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/PEBPapuaUI/UserControl/UserControlEmploySearch.ascx" TagName="UserControlEmploySearch" TagPrefix="uc2" %>
<%@ Register Src="~/PEBPapuaUI/UserControl/GeneralTitleHeader.ascx" TagPrefix="uc1" TagName="GeneralTitleHeader" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" oncontextmenu="return false;">
<head runat="server">
    <title>Event Handling Attendance Detail</title>


    <link href="../../../../PapuaStyle/GeneralStyle/PapuaControlStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../../../PapuaStyle/GeneralStyle/PapuaGeneralStyle.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .auto-style59 {
            width: 12%;
        }
        .auto-style60 {
            width: 100px;
        }
        .auto-style61 {
            width: 2px;
        }
        .auto-style62 {
            width: 465px;
        }
        .auto-style63 {
            width: 500%;
        }
        </style>

    <script src="../../../../JavaScript/html5-qrcode-master/minified/html5-qrcode.min.js"></script>
    <script src="../../../../Scripts/jquery-1.8.2.js" type="text/javascript"></script>
    <script src="../../../../Scripts/jquery-ui-1.8.24.js" type="text/javascript"></script> 


    
    
</head>
<body class="papuaBody">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager" runat="server" AsyncPostBackTimeout="0">
        </asp:ScriptManager>

        <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>--%>
        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <uc1:GeneralTitleHeader runat="server" ID="GeneralTitleHeader" />

                <table style="width: 100%;">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnClose" runat="server" CssClass="btnClose" OnClick="btnClose_Click" TabIndex="100"  />
                        </td>
                    </tr>
                </table>

                <asp:Panel ID="PanelEvent" runat="server" style="width: 100%">
                    <table class="papuaTable" style="width: 100%">
                        <tr>
                            <td height="10pt" >Program Title</td>
                            <td height="4pt" >:</td>
                            <td >
                                <asp:Label ID="lbProgTitle" runat="server" Font="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lbProgGroupName" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                           <td rowspan="5" width="35%" height="145pt" style="text-align:center; font-size:large">
                                     <asp:Label ID="lbStatus" runat="server" Text='<%# Bind("Employee_Name") %>' ForeColor="Green" ></asp:Label>
                                       <br /> <asp:Image  runat="server" ID="imgFoto" Width="120px" Height="150px"/><br />
                                <asp:Button ID="btnApprove" runat="server" CssClass="btnBlue" Text="Approve" ToolTip="Insert " OnClick="btnApprove_Click" />
                                <asp:Button ID="btnReject" runat="server" CssClass="btnBlue" Text="Reject" ToolTip="Reject " OnClick="btnReject_Click" />
                            
                              
                            </td>
                        </tr>
                        <tr>
                            <td  height="16pt"">Training Period</td>
                            <td height="4pt" >:</td>
                            <td >
                                <asp:Label ID="lbStartTime" runat="server" Font="true" ForeColor="Blue"></asp:Label>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TO &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lbEndTime" runat="server" Font="true" ForeColor="Blue"></asp:Label>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblCount" runat="server" Font="true" ForeColor="Red">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:Label>

                                <asp:Label ID="lblDays" runat="server" Font="true" ForeColor="Red">Days</asp:Label>
                            </td> 
                          
                    </tr>
                        <tr>
                            <td height="16pt" class="auto-style59">Venue</td>
                            <td width="1%" height="16pt">:</td>
                            <td>
                                <asp:Label ID="lbVenue" runat="server" Font="True"></asp:Label>
                            </td> 
                        </tr>
                        <tr>
                            <td height="16pt" class="auto-style59">Provider</td>
                            <td width="1%" height="16pt">:</td>
                            <td>
                                <asp:Label ID="lbProvider" runat="server" Font="True"></asp:Label>
                            </td> 
                        </tr>
                        <tr>
                            <td height="16pt" class="auto-style59">Trainer</td>
                            <td width="1%" height="16pt">:</td>
                            <td>
                                <asp:Label ID="lbTrainer" runat="server" Font="True"></asp:Label>
                            </td> 
                        </tr>
                        <tr>
                            <td height="16pt" class="auto-style59">Participant /Att</td>
                            <td width="1%" height="16pt">:</td>
                            <td>
                                <asp:Label ID="lbParticipantAtt" runat="server" Font="True"></asp:Label>
                            </td> 
                            <td>
                                <asp:CheckBox ID="cbAllow" runat="server" Value="1" Text="Allow Unregister Participant" />
                            </td>
                            
                        </tr>
                        
                    </table>
                </asp:Panel>

              
               
                    <table class="papuaTable" style="width: 100%">
                        <tr>
                            <td height="16pt" class="auto-style60">Date Attendance</td>
                            <td height="2pt" class="auto-style61">:</td>
                            <td class="auto-style62">
                                <asp:Label ID="lbDateAtten" runat="server" Font="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lbharike" runat="server" Font="True"></asp:Label>
                            </td>
                            
                            <td width="35%">
                                <asp:Label ID="Label1" CssClass="general"  Text="Scan QR Code: " runat="server" ></asp:Label>
                                <asp:TextBox ID="tbQR" runat="server" Width="50%" OnTextChanged="tbQR_TextChanged" AutoPostBack="false"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblqr" CssClass="general" ForeColor="Red" Text="*" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblName" CssClass="general" Text="" runat="server" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblEnterCount" CssClass="general" Text="" runat="server" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="ScanQR" runat="server" Text="Scan QR by Camera" Visible="false" OnClick="ScanQR_Click" CssClass="btnBlue"  />
                            </td>
                        </tr>
                        </table>        
                <br />
                <asp:Panel ID="PanelTrainAttend" runat="server" Enabled="true">
                       <asp:GridView ID="GVAttPivot" runat="server" Width="100%" CssClass="papuagrid" AutoGenerateColumns="true" AllowSorting="True"
                        OnRowDataBound="gv_RowDataBound">
                            <Columns><asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                        <asp:HiddenField ID="lblNo" runat="server" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                </asp:TemplateField></Columns>
                       </asp:GridView>
                       


                    <br />
                    <asp:GridView ID="gvtrainatten" runat="server" Width="100%" CssClass="papuagrid" AutoGenerateColumns="false" AllowSorting="True"
                        OnRowDataBound="gv_RowDataBound" Visible="false">
                        <Columns>

                            <asp:TemplateField HeaderText="No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"  ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Emp.No">
                                <ItemTemplate>
                                    <asp:Label ID="lbempno" runat="server" Text='<%# Bind("employee_no") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Emp.Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbempname" runat="server" Text='<%# Bind("Employee_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dept">
                                <ItemTemplate>
                                    <asp:Label ID="lbdept" runat="server" Text='<%# Bind("Dept") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Section">
                                <ItemTemplate>
                                    <asp:Label ID="lbsection" runat="server" Text='<%# Bind("DeptSection") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Line">
                                <ItemTemplate>
                                    <asp:Label ID="lbline" runat="server" Text='<%# Bind("DeptLine") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="Model">
                                <ItemTemplate>
                                    <asp:Label ID="lbmodel" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Station">
                                <ItemTemplate>
                                    <asp:Label ID="lbstation" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                          <%--  <asp:TemplateField HeaderText="Attend">
                                <ItemTemplate>
                                    <asp:Label ID="lbattend" runat="server" Text='<%# Bind("AttDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                        </Columns>
                        <PagerStyle CssClass="pgr" HorizontalAlign="Center" />
                        <HeaderStyle ForeColor="White" />
                    </asp:GridView>

                </asp:Panel>

                <br />

                 <div>
                     
                     <table style="width: 100%;">
                         <tr>
                             <td align="right" width="20%">
                           
                        </td>
                             <td align="center" Width="20%">
                                 <asp:Label runat="server" Text=" !!!Under Development!!!" ForeColor="red"></asp:Label>

                             </td>
                             <td align="right" width="20%">
                           
                        </td>
                         </tr>
                    <tr>
                        <td align="right" width="20%">
                           
                        </td>
                        <td align="center">
                            <div id="qr-reader" style="width: 400px"></div>
                        </td>
                        <td align="right" width="20%">
                           
                        </td>
                    </tr>
                </table>
                     
                     
       

                    <asp:HiddenField ID="hf_EventID" runat="server" />
                    <asp:HiddenField ID="hf_ProgID" runat="server" />
                    <asp:HiddenField ID="hf_ItpID" runat="server" />

                    <asp:HiddenField ID="hf_HuroDeptID" runat="server" />
                    <asp:HiddenField ID="hf_divisi" runat="server" />
                    <asp:HiddenField ID="hf_HuroDivisiID" runat="server" />
                    <asp:HiddenField ID="hf_ro" runat="server" />
                    <asp:HiddenField ID="hf_AllDataBool" runat="server" />
                    <asp:HiddenField ID="hf_modeView" runat="server" />
                    <asp:HiddenField ID="hf_ipaddress" runat="server" />
                    <asp:HiddenField ID="hf_superiorID" runat="server" />
                    <asp:HiddenField ID="hf_empno" runat="server" />
                    <asp:HiddenField ID="hf_empname" runat="server" />
                    <asp:HiddenField ID="hf_loginTime" runat="server" />
                    <asp:HiddenField ID="hf_deptID" runat="server" />
                    <asp:HiddenField ID="hf_dept" runat="server" />
                    <asp:HiddenField ID="hf_userlevel" runat="server" />
                    <asp:HiddenField ID="hf_taskID" runat="server" />
                    <asp:HiddenField ID="hf_taskName" runat="server" />
                    <asp:HiddenField ID="hf_menuID" runat="server" />
                    <asp:HiddenField ID="hf_userID" runat="server" />
                    <asp:HiddenField ID="hf_Add" runat="server" />
                    <asp:HiddenField ID="hf_Update" runat="server" />
                    <asp:HiddenField ID="hf_delete" runat="server" />
                    <asp:HiddenField ID="hf_PageNo" runat="server" />
                    <asp:HiddenField ID="hf_Row" runat="server" />
                    <asp:HiddenField ID="hf_AppTaskStep" runat="server" />
                    <asp:HiddenField ID="hf_AppStep" runat="server" />
                    <asp:HiddenField ID="hf_tabIndex" runat="server" />
                    <asp:HiddenField ID="hf_Param1" runat="server" />
                    <asp:HiddenField ID="hf_Param2" runat="server" />

                    <asp:HiddenField ID="hf_FileNameToUpload" runat="server" />
                    <asp:HiddenField ID="hf_memberID" runat="server" />
                    <asp:HiddenField ID="hf_ModulName" runat="server" />
                    <asp:HiddenField ID="hf_folderUploadDownload" runat="server" />
                    <asp:HiddenField ID="Hf_YearUploadDonlot" runat="server" />
                    <asp:HiddenField ID="hf_fileAttach" runat="server" />
                     <asp:HiddenField ID="hf_onlineID" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="updatePanel"
            DisplayAfter="0" EnableViewState="true">
            <ProgressTemplate>
                <div id="Div2" align="center" valign="bottom" runat="server" style="position: fixed; top: 0; right: 0; bottom: 0; left: 0; visibility: visible; vertical-align: bottom; border-width: thin; border-color: transparent; border-bottom-style: solid; background-color: transparent; width: 100%; height: 200%; filter: alpha(opacity=20); width: 100%; height: 100%">
                    <div id="Div3" align="center" valign="bottom" runat="server" style="position: fixed; top: 1%; visibility: visible; background-color: transparent; width: 20%; left: 40%; top: 25%">
                        <img src="../../../../PapuaImages/Progress/indicator.white.gif" alt="Loading" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <script type="text/javascript">
            var separCount = 0;
            var EnterCount = 0;
            var qrCodeScanner;
            qrCodeScanner = new Html5QrcodeScanner("qr-reader", {
                fps: 10, qrbox: 170, useBarCodeDetectorIfSupported: true
            });
            //$(document).ready(function () {
                

                qrCodeScanner.render(onScanSuccess);
            //});

            function onScanSuccess(decodedText, decodedResult) {

                const myArray = decodedText.split(";");
                let word = myArray[0];
                alert(word);
                CheckScanResult(word);
                //qrCodeScanner.clear();
            }
            
            $("#tbQR").keypress(function (e) {
                var dept = $("#tbQR").val();
                if (e.which == 13) {
                    EnterCount++;
                    if (EnterCount == 6) {
                        var mylabel = document.getElementById("lblName");
                        var str = $("#tbQR").val();

                        
                        $("#lblName").text(str);
                        EnterCount = 0;
                        
                        // __doPostBack("tbQR", "TextChanged");
                        CheckScanResult(str);
                        $("#tbQR").val("");
                        

                    }
                    $("#lblEnterCount").text(EnterCount);
                    e.preventDefault();
                }
                else if (e.which == 59) {
                    separCount++;

                }
                else {
                }
            });

            function doback()
            {
                __doPostBack("Page", "Load");
            }
             function CheckScanResult (EmpNo){
                
                 var myEventID = document.getElementById('<%= hf_EventID.ClientID %>').value
                 //var myAllow = document.getElementById<%--('<%=cbAllow.ClientID%>')--%>.value;
                 //var x = $("#checkbox").is(":checked");
                 var x = document.getElementById("cbAllow").checked

                 //alert(document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.value);
                $.ajax({
                    url: "EventHandlingAttendanceDetail.aspx/CheckPartisipant",
                    async: true,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({ EmpNo: EmpNo, EventID: myEventID, AllowUnregist: x }),
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        var myRes = data.d.split('|')
                        //alert(myRes[0]);
                        if (myRes[0] == '[SUCCESS]')
                        {
                            var cookName = 'lastParti' + myEventID;
                            document.cookie = cookName + "=" + myRes[0] + "|" + EmpNo;

                            //alert(data.d); //Success | [...] | [...]
                            $("#lbStatus").text(data.d);
                            document.getElementById('lbStatus').setAttribute('style', 'color: green;');
                          
                            setTimeout(doback(),3000);
                         
                           
                         
                           //document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: green;');
                        }
                        else if (myRes[0] == '[EXISTING]') {
                            $("#lbStatus").text(data.d);

                            document.getElementById('lbStatus').setAttribute('style', 'color: orange;');
  
        
                            const img = document.getElementById('imgFoto');
                            img.setAttribute('src', '');
                        }

                        //else if (myRes[0] == '[UNREGISTER]') {
                        else{
                            $("#lbStatus").text(data.d);
                            
                            document.getElementById('lbStatus').setAttribute('style', 'color: red;');

                            //document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: red;');
                        }
                        //else alert('Unregistered...');
                        
                        

                    }, error: function (xhr, status, error) {
                        //var msg = JSON.parse(status);
                        alert('no data found ')

                        document.getElementById('lbStatus').setAttribute('style', 'color: orange;');
                        //document.getElementByI<%--d('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: yellow;');
                    },
                });
            } 
        </script>

        

    </form>
</body>
</html>
