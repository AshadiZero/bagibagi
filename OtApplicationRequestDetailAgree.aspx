﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OtApplicationRequestDetailAgree.aspx.cs" Inherits="UI_AttendanceOT_Transaction_OtApplicationRequestDetailAgree" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" oncontextmenu="return false;">
<head runat="server">
    <title>Over Time Form</title>
    <link href="../../../Styles/PapuaControlStyle.css" rel="stylesheet" type="text/css" />
<%--    <link href="../../../Styles/PapuaGeneralStyle.css" rel="stylesheet" type="text/css" />--%>
        <link rel="stylesheet" type="text/css" href="../../../Content/BorneoStyle.css" /> 
    <link href="../../../Content/Themes/carano/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
    <link type="text/css"  rel="Stylesheet" href="../../../Content/Css/PityControlStyle.css" />

    <script language="javascript" type="text/javascript">
        var counter = 0;

        var pattern = 'GV01';

        // Get the checkboxes inside the Gridview which is part of the template column
        function GetChildCheckBoxCount() {
            var checkBoxCount = 0;

            var elements = document.getElementsByTagName("INPUT");

            for (i = 0; i < elements.length; i++) {
                if (IsCheckBox(elements[i]) && IsMatch(elements[i].id)) checkBoxCount++;
            }

            return parseInt(checkBoxCount);
        }

        // A function that checks if the checkboxes are the one inside the GridView 
        function IsMatch(id) {
            var regularExpresssion = new RegExp(pattern);

            if (id.match(regularExpresssion)) return true;
            else return false;
        }

        function IsCheckBox(chk) {
            if (chk.type == 'checkbox') return true;
            else return false;
        }


        function AttachListener() {
            var elements = document.getElementsByTagName("INPUT");

            for (i = 0; i < elements.length; i++) {
                if (IsCheckBox(elements[i]) && IsMatch(elements[i].id)) {
                    AddEvent(elements[i], 'click', CheckChild);
                }
            }
        }

        function CheckChild(e) {
            var evt = e || window.event;

            var obj = evt.target || evt.srcElement

            if (obj.checked) {
                if (counter < GetChildCheckBoxCount())
                { counter++; }
            }

            else {
                if (counter > 0) { counter--; }
            }

            //            if (counter == GetChildCheckBoxCount())
            //            { document.getElementById("chkAll").checked = true; }
            //            else if (counter < GetChildCheckBoxCount()) { document.getElementById("chkAll").checked = false; }

        }

        function AddEvent(obj, evType, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evType, fn, true);
                return true;
            }

            else if (obj.attachEvent) {
                var r = obj.attachEvent("on" + evType, fn);
                return r;
            }
            else {
                return false;
            }
        }


        function Check(parentChk) {
            var elements = document.getElementsByTagName("INPUT");

            for (i = 0; i < elements.length; i++) {
                if (parentChk.checked == true) {
                    if (IsCheckBox(elements[i]) && IsMatch(elements[i].id)) {
                        elements[i].checked = true;
                    }
                }
                else {
                    elements[i].checked = false;
                    // reset the counter 
                    counter = 0;
                }
            }

            if (parentChk.checked == true) {
                counter = GetChildCheckBoxCount();
            }
        }

        function expandcollapse(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/minimize.png";
                }
                else {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/minimize.png";
                }
                img.alt = "Close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/maximize.png";
                }
                else {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/maximize.png";
                }
                img.alt = "Expand";
            }
        }


    </script>
    <script language="javascript" type="text/javascript">
        var counter2 = 0;

        var pattern2 = 'GV02';

        // Get the checkboxes inside the Gridview which is part of the template column
        function GetChildCheckBoxCount2() {
            var checkBoxCount2 = 0;

            var elements = document.getElementsByTagName("INPUT");

            for (i = 0; i < elements.length; i++) {
                if (IsCheckBox2(elements[i]) && IsMatch2(elements[i].id)) checkBoxCount2++;
            }

            return parseInt(checkBoxCount2);
        }

        // A function that checks if the checkboxes are the one inside the GridView 
        function IsMatch2(id) {
            var regularExpresssion = new RegExp(pattern2);

            if (id.match(regularExpresssion)) return true;
            else return false;
        }

        function IsCheckBox2(chk) {
            if (chk.type == 'checkbox') return true;
            else return false;
        }


        function AttachListener2() {
            var elements = document.getElementsByTagName("INPUT");

            for (i = 0; i < elements.length; i++) {
                if (IsCheckBox2(elements[i]) && IsMatch2(elements[i].id)) {
                    AddEvent2(elements[i], 'click', CheckChild2);
                }
            }
        }

        function CheckChild2(e) {
            var evt = e || window.event;

            var obj = evt.target || evt.srcElement

            if (obj.checked) {
                if (counter2 < GetChildCheckBoxCount2())
                { counter2++; }
            }

            else {
                if (counter2 > 0) { counter2--; }
            }

            if (counter2 == GetChildCheckBoxCount2())
            { document.getElementById("chkAll2").checked = true; }
            else if (counter2 < GetChildCheckBoxCount2()) { document.getElementById("chkAll2").checked = false; }

        }

        function AddEvent2(obj, evType, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evType, fn, true);
                return true;
            }

            else if (obj.attachEvent) {
                var r = obj.attachEvent("on" + evType, fn);
                return r;
            }
            else {
                return false;
            }
        }


        function Check2(parentChk) {
            var elements2 = document.getElementsByTagName("INPUT");

            for (i = 0; i < elements2.length; i++) {
                if (parentChk.checked == true) {
                    if (IsCheckBox2(elements2[i]) && IsMatch2(elements2[i].id)) {
                        elements2[i].checked = true;
                    }
                }
                else {
                    elements2[i].checked = false;
                    // reset the counter 
                    counter2 = 0;
                }
            }

            if (parentChk.checked == true) {
                counter2 = GetChildCheckBoxCount2();
            }
        }

        function expandcollapse2(obj, row) {
            var div = document.getElementById(obj);
            var img = document.getElementById('img' + obj);

            if (div.style.display == "none") {
                div.style.display = "block";
                if (row == 'alt') {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/minimize.png";
                }
                else {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/minimize.png";
                }
                img.alt = "Close";
            }
            else {
                div.style.display = "none";
                if (row == 'alt') {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/maximize.png";
                }
                else {
                    img.src = "../../../PapuaImages/GeneralControl/PapuaMisc/maximize.png";
                }
                img.alt = "Expand";
            }
        }


    </script>

    <script language="javascript" type="text/javascript">
        function aku(jenis) {
            //alert(jenis);
            if (jenis == 'Approve') {
                alert('Processing Approve Data');
                //document.getElementById('Button1').visible = false;

            }
            else {
                alert('Processing Reject Data');
            }
        }

    </script>


    <style type="text/css">
        .style2 {
            width: 231px;
        }

        .style3 {
            width: 185px;
        }

        .style5 {
            height: 21px;
            width: 50px;
        }

        .style6 {
            height: 21px;
            width: 6px;
        }

        .style10 {
            height: 21px;
            width: 79px;
        }

        .style11 {
            width: 79px;
        }

        .style14 {
            height: 21px;
            width: 185px;
        }

        .style19 {
            width: 88px;
        }

        .style20 {
            width: 4px;
        }

        .style21 {
            width: 115px;
        }

        .style22 {
            width: 13px;
        }

        .style23 {
            height: 21px;
            width: 2px;
        }

        .style24 {
            height: 21px;
            width: 81px;
        }

        .style25 {
            width: 81px;
        }

        .style28 {
            width: 5px;
        }

        .style31 {
            height: 21px;
            width: 1px;
        }

        .style32 {
            width: 1px;
            height: 1px;
        }
        .auto-style1 {
            width: 20px;
        }
    </style>

</head>
<body class="pityBody ui-widget-content" onload="AttachListener();AttachListener2();">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="0">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="UPUtama" runat="server">
            <ContentTemplate>
                      <div class="header2" align="center">                
                         
                        <span id="Span5" style="font-family:Tahoma;font-size:14pt; color:#55a3d9">
                        <asp:Label ID="lblTitle" runat="server" ></asp:Label>
                        </span>
                     </div>
                <asp:Panel ID="UPpriod" runat="server">
                    <table style="width: 100%;" class="papuaTable">
                        <tr>
                            <td class="style19">
                                <asp:Label ID="label5" runat="server" CssClass="general" Text="Report Periode"></asp:Label>
                            </td>
                            <td class="style20">:
                            </td>
                            <td class="style21">
                                <b>
                                    <%--<asp:TextBox ID="tbDateFrom" runat="server" CssClass="general" Width="150px"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="tbDateFrom">
                            </cc1:CalendarExtender>--%></b> <b>
                                <asp:Label ID="lblDateFrom" runat="server" CssClass="general"
                                    Text='<%# Bind("date_from") %>'></asp:Label>
                            </b>
                            </td>
                            <td class="style22">
                                <asp:Label ID="label7" runat="server" CssClass="general" Text="To"></asp:Label>
                            </td>
                            <td class="style23">:
                            </td>
                            <td>
                                <b>
                                    <%-- <asp:TextBox ID="tbDateTo" runat="server" CssClass="general" Width="150px"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="tbDateTo">
                            </cc1:CalendarExtender>--%></b> <b>
                                <asp:Label ID="lblDateTo" runat="server" CssClass="general"
                                    Text='<%# Bind("date_to") %>'></asp:Label>
                            </b>
                            </td>
                        </tr>
                    </table>

                </asp:Panel>
                 
                <%--<uc1:GeneralTitleHeader runat="server" ID="GeneralTitleHeader" />--%>
                <asp:Panel ID="pnlClose" runat="server">
                    <div id="divCloseForm" align="right" style="width: 100%">
                        <asp:Button ID="btnClose" runat="server" CssClass="btnExitIcongambar" OnClick="btnClose_Click" />
                    </div>
                </asp:Panel>

                <asp:Panel Width="100%" runat="server" GroupingText="OT" ID="panelForm" CssClass="papuaTable">
                    <table style="width: 100%; height: 149px; text-align: center;">

                        <tr>
                            <td style="text-align: left;" class="style10">
                                <asp:Label ID="Label3" runat="server" CssClass="general" Text="RefNo"></asp:Label>
                            </td>
                            <td class="style31">
                                <asp:Label ID="Label2" runat="server" Text=":" CssClass="general"></asp:Label></td>
                            <td style="text-align: left;" class="style2">
                                <asp:Label ID="lblrefID" runat="server" CssClass="general"
                                    Text='<%# Bind("RefNo") %>'></asp:Label>
                            </td>
                            <td style="text-align: left;" class="style24">Proposed By
                            </td>
                            <td class="style28">
                                <asp:Label ID="Label4" runat="server" Text=":" CssClass="general"></asp:Label></td>
                            <td style="text-align: left;" class="style14">
                                <asp:Label ID="lblProposedBy" runat="server" CssClass="general"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" class="style11">Division</td>
                            <td class="style32">
                                <asp:Label ID="Label6" runat="server" Text=":" CssClass="general"></asp:Label></td>
                            <td style="text-align: left;" class="style2">
                                <asp:Label ID="lblDivision" runat="server" CssClass="general"></asp:Label>
                                <asp:Label ID="lbldivID" runat="server" CssClass="general" Visible="false"></asp:Label>
                            </td>
                            <td style="text-align: left;" class="style25">Date</td>
                            <td class="style28">
                                <asp:Label ID="lbl0" runat="server" CssClass="general" Text=":"></asp:Label>
                            </td>
                            <td style="text-align: left;" class="style14">
                                <asp:TextBox ID="txtDate" runat="server" AutoPostBack="True"
                                    CommandName="cmdAIID" CssClass="general" Font-Bold="False"
                                    OnTextChanged="txtDate_TextChanged " Width="90px" Height="16px" autocomplete="off"></asp:TextBox>
                                <asp:Label ID="lblHoliday" runat="server" CssClass="general"></asp:Label>
                                <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server"
                                    CssClass="pitycalblue" Enabled="True" Format="yyyy-MM-dd"
                                    PopupButtonID="txtDate" TargetControlID="txtDate"></cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="style11">Department </td>
                            <td class="style32">
                                <asp:Label ID="Label21" runat="server" Text=":" CssClass="general"></asp:Label></td>
                            <td style="text-align: left" class="style2">
                                <asp:Label ID="lblDepartment" runat="server" CssClass="general" Visible="false"></asp:Label>
                                <asp:DropDownList ID="ddlDept" runat="server" AutoPostBack="true"
                                    CssClass="general" Font-Bold="False"
                                    OnSelectedIndexChanged="ddlDept_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:Label ID="lbldeptcadangan" runat="server" CssClass="general" Visible="false"></asp:Label>
                            </td>
                            <td class="style25" style="text-align: left">
                                <asp:Label ID="lbl" runat="server" CssClass="general" Text="Time"></asp:Label>
                            </td>
                            <td class="style28">
                                :
                            </td>
                            <td style="text-align: left" class="style3">
                                <asp:UpdatePanel ID="UPTime" runat="server">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtstarttime" runat="server" CssClass="general" MaxLength="5"
                                                        OnTextChanged="txtstarttime_TextChanged" ValidationGroup="vgOT"
                                                        Width="40px" Height="16px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="txtEndTime_MaskedEditExtender" runat="server"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                                        CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"
                                                        Mask="99:99" MaskType="Time" TargetControlID="txtEndTime"
                                                         AcceptAMPM="false"
                                                        UserTimeFormat="TwentyFourHour"></cc1:MaskedEditExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblto" runat="server" Text="To"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEndTime" runat="server" AccessKey="E" AutoPostBack="true"
                                                        CssClass="general" MaxLength="5" OnTextChanged="txtEndTime_TextChanged"
                                                        ValidationGroup="vgOT" Width="40px" Height="16px"></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="txtstarttime_MaskedEditExtender" runat="server"
                                                        AcceptAMPM="false" CultureAMPMPlaceholder=""
                                                        CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                                        CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"
                                                        Mask="99:99" MaskType="Time" TargetControlID="txtstarttime"
                                                        UserTimeFormat="TwentyFourHour"></cc1:MaskedEditExtender>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDuration" runat="server" CssClass="general" Visible="False"></asp:Label>
                                                    <asp:Label ID="lbltitik" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>





                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="style11">Section</td>
                            <td class="style31">
                                <asp:Label ID="Label12" runat="server" Text=":" CssClass="general"></asp:Label></td>
                            <td style="text-align: left" class="style2">
                                <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true"
                                    CssClass="general" Font-Bold="False"
                                    OnSelectedIndexChanged="ddlSection_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="ddlSection" ErrorMessage="Please choose Section"
                                    ValidationGroup="ValidationSearch"></asp:RequiredFieldValidator>
                            </td>
                            <td class="style25" style="text-align: left" rowspan="2">Task</td>
                            <td class="style28" rowspan="2">:</td>
                            <td style="text-align: left" class="style3" rowspan="2">


                                <asp:TextBox ID="txtTask" runat="server" AccessKey="E" CssClass="general" Height="30px"
                                    TextMode="MultiLine" Width="200px"></asp:TextBox>


                            </td>
                        </tr>
                        <tr>
                            <td class="style11" style="text-align: left">Line</td>
                            <td class="style32">:</td>
                            <td class="style2" style="text-align: left">
                                <asp:DropDownList ID="ddlLine" runat="server" AutoPostBack="true"
                                    CssClass="general" Font-Bold="False"
                                    OnSelectedIndexChanged="ddlLine_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                    ControlToValidate="ddlLine" ErrorMessage="Please choose Line"
                                    ValidationGroup="ValidationSearch"></asp:RequiredFieldValidator>
                                &nbsp;
    <asp:DropDownList ID="ddCalTypeID" runat="server" AutoPostBack="true"
        CssClass="general" Font-Bold="False" Visible="false">
    </asp:DropDownList>
                                <asp:Label ID="lblCalTypeName" runat="server" CssClass="general" Visible="true"></asp:Label>
                                <asp:Label ID="lblCaltypeID" runat="server" CssClass="general" Visible="false"></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td class="style11" style="text-align: left">Shift Code</td>
                            <td class="style31">:</td>
                            <td class="style2" style="text-align: left">
                                <asp:DropDownList ID="ddlShiftCode" runat="server" AutoPostBack="true"
                                    CssClass="general" Font-Bold="False"
                                    OnSelectedIndexChanged="ddlShiftCode_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                    ControlToValidate="ddlShiftCode" ErrorMessage="Please choose Shift Code"
                                    ValidationGroup="ValidationSearch"></asp:RequiredFieldValidator>
                            </td>
                            <td class="style25" style="text-align: left" rowspan="2">
                                <asp:Label ID="lblnextApp" runat="server" CssClass="general" Visible="false"></asp:Label>
                            </td>
                            <td class="style28" rowspan="2">
                                <asp:Label ID="lbltitikApp" runat="server" CssClass="general" Text=":"
                                    Visible="false"></asp:Label>
                            </td>
                            <td style="text-align: left" class="style3" rowspan="2">
                                <asp:DropDownList ID="DdSuperior" runat="server" CssClass="general" AutoPostBack="true"
                                    OnSelectedIndexChanged="DdSuperior_SelectedIndexChanged" Visible="false">
                                </asp:DropDownList>
                                <asp:Label ID="LbSuperior" runat="server" CssClass="general" Visible="false"></asp:Label>
                            </td>

                            <%--<td class="style9">
                                    <asp:Label ID="lblnextApp" runat="server" CssClass="general" Text="Next Approval"></asp:Label>
                                </td>
                                <td class="style8">
                                    <asp:Label ID="lbtitik" runat="server" CssClass="general" Text=":"></asp:Label>
                                    </td>
                                <td class="style1" style="text-align: left" colspan="3">
                                      <asp:DropDownList ID="DdSuperior" runat="server" 
                                        CssClass="general">
                                    </asp:DropDownList>
                                   <asp:Label ID="LbSuperior" runat="server" CssClass="general"></asp:Label>
                                   <asp:HiddenField ID="hf_SuperiorId" runat="server" />
                                    <br />
                                </td>--%>
                        </tr>
                    </table>
                </asp:Panel>
                <div align="right" style="height: 20px">
                    <asp:Button ID="Button1" runat="server" Text="finger" OnClick="Button1_Click" Visible="false" />
                    <asp:Button ID="btnSearch0" runat="server" CssClass="btnBlue"
                        OnClick="btnSearch0_Click" Text="Search Employee" Height="20px"
                        ValidationGroup="ValidationSearch" Visible="false" Width="121px" />
                    <asp:Button ID="btnSave" runat="server" Text="Save" CommandName="Simpan" Width="75px" CssClass="btnBlue" Height="20px" OnClick="Btn_Command" />
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" Visible="false" Width="75px" CssClass="btnBlue" Height="20px" OnClick="Btn_Command" />
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CommandName="Submit" Visible="false" Width="75px" CssClass="btnBlue" Height="20px" OnClick="Btn_Command" />
                    <asp:Button ID="btnApprove" runat="server" Text="Approve" CommandName="Approve" Visible="false" Width="75px" CssClass="btnBlue" Height="20px" OnClick="Btn_Command" />
                    <%--OnClientClick="aku('Approve')"--%>
                    <asp:Button ID="btnReject" runat="server" Text="Reject" CommandName="Reject" Visible="false" Width="75px" CssClass="btnBlue" Height="20px" OnClick="Btn_Command" />
                </div>
                <asp:HiddenField ID="hf_SuperiorId" runat="server" Visible="false" />
                <asp:HiddenField ID="HF_OTDTID" runat="server" />
                <asp:Label ID="lblOTForm" runat="server" CssClass="general" Visible="false"></asp:Label>


                <asp:Panel Width="100%" runat="server" GroupingText="OT Employee List" ID="panelList">
                    <asp:Panel ID="pnlexcell" runat="server">
                        <div align="right" style="height: 20px">
                            <table>
                                <tr align="right">
                                    <td align="right" class="auto-style1">
                                        <asp:Button ID="btnexcell" runat="server" CssClass="btnExitIcongambar"
                                            OnClick="btnexcell_Click" Height="20px" Width="20px" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div id="divku" runat="server">

                        <asp:Label ID="lblNote" runat="server" CssClass="general" Visible="false" Text="Emp_No Color Note : " ForeColor="#ff66b3" Font-Size="XX-Small"></asp:Label>
                        <asp:Label ID="lblg1" runat="server" CssClass="general" Text="Group-1 Holiday | " Visible="false" ForeColor="#00b33c" Font-Size="XX-Small"></asp:Label>
                        <asp:Label ID="lblg2" runat="server" CssClass="general" Text="Group-2 Holiday | " Visible="false" ForeColor="#ff8080" Font-Size="XX-Small"></asp:Label>
                        <asp:Label ID="lblg3" runat="server" CssClass="general" Text="Group-3 Holiday" Visible="false" ForeColor="#b366ff" Font-Size="XX-Small"></asp:Label>

                        <asp:GridView ID="GV01" runat="server"
                            Font-Size="8pt"
                            HorizontalAlign="Left" AutoGenerateColumns="False"
                            OnSorting="GV_Sorting" AllowSorting="True"
                            OnRowDataBound="GV_RowDataBound"
                            OnRowCommand="GV_RowCommand" Width="100%" CssClass="papuagrid"
                            HeaderStyle-HorizontalAlign="Left"
                            RowStyle-HorizontalAlign="Left" OnPageIndexChanging="GV_PageIndexChanging">
                            <HeaderStyle Font-Bold="true" />
                            <Columns>
                                <asp:TemplateField HeaderText="otdetailsid" Visible="False">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblDetailsID" runat="server" Text='<%# Bind("OTDetailsID") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDetailsID" runat="server" Text='<%# Bind("OTDetailsID") %>'></asp:Label>
                                        <asp:Label ID="lblcaltypeID" runat="server" Text='<%# Bind("calTypeID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emp No" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("employeeNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("employeeNo") %>'></asp:Label>
                                    </EditItemTemplate>

                                    <HeaderStyle HorizontalAlign="Left" />

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emp Name" HeaderStyle-HorizontalAlign="Left">

                                    <HeaderStyle HorizontalAlign="Left" />

                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("employee_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("employee_name") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <FooterStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Plan T.From" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:TextBox ID="txtPTimeFrom" Text='<%# Bind("planFromTime") %>' runat="server"
                                            Width="40px" MaxLength="5" Font-Bold="False" ValidationGroup="vgOT"
                                            AutoPostBack="True" CssClass="general"
                                            OnTextChanged="txtPTimeFrom_TextChanged"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtPTimeFrom_MaskedEditExtender" runat="server"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                            CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"
                                            Mask="99:99" MaskType="Time" TargetControlID="txtPTimeFrom"
                                            UserTimeFormat="TwentyFourHour"></cc1:MaskedEditExtender>
                                        <asp:Label ID="lblPTimeFrom" runat="server" Text='<%# Bind("planFromTime") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Plan T.To" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>

                                        <asp:TextBox ID="txtPTimeTo" Text='<%# Bind("planToTime") %>' runat="server"
                                            Width="40px" Font-Bold="False" MaxLength="5" AutoPostBack="True"
                                            ValidationGroup="vgOT" CssClass="general"
                                            OnTextChanged="txtTo_TextChanged"></asp:TextBox>
                                        <asp:Label ID="lblPTimeTo" runat="server" Text='<%# Bind("planToTime") %>' Visible="false"></asp:Label>
                                        <cc1:MaskedEditExtender ID="txtTo_MaskedEditExtender" runat="server"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                            CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"
                                            Mask="99:99" MaskType="Time" TargetControlID="txtPTimeTo"
                                            UserTimeFormat="TwentyFourHour"></cc1:MaskedEditExtender>

                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Plan Drtn" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPDuration" Text='<%# Bind("planDuration") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Actual_Time_From"  HeaderStyle-HorizontalAlign="Left">                                       
                                                <ItemTemplate>
                                           
                                                    <asp:TextBox ID="txtActualTimeFrom" Text='<%# Bind("actualTimeFrom") %>' runat="server"
                                                        Width="40px" MaxLength="5" Font-Bold="False" ValidationGroup="vgOT" 
                                                        AutoPostBack="True"  CssClass="general" ></asp:TextBox>
                                                    <cc1:MaskedEditExtender ID="txtActualTimeFrom_MaskedEditExtender" runat="server" 
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                                                        CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                                                        Mask="99:99" MaskType="Time" TargetControlID="txtActualTimeFrom" 
                                                        UserTimeFormat="TwentyFourHour">
                                                    </cc1:MaskedEditExtender>
                                                    <asp:Label ID="lblActualTimeFrom" runat="server" Text='<%# Bind("actualTimeFrom") %>'  Visible="false" ></asp:Label>
                                                </ItemTemplate>                                       
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="Actual T.From" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtActualTimeFrom" Text='<%# Bind("actualTimeFrom") %>' runat="server"
                                            Width="100px" MaxLength="5" Font-Bold="False" ValidationGroup="vgOT"
                                            AutoPostBack="True" CssClass="general" Visible="false"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtActualTimeFrom_MaskedEditExtender" runat="server"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                            CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"
                                            Mask="99:99" MaskType="Time" TargetControlID="txtActualTimeFrom"
                                            UserTimeFormat="TwentyFourHour"></cc1:MaskedEditExtender>
                                        <asp:Label ID="lblActualTimeFrom" runat="server" Text='<%# Bind("actualTimeFrom") %>' ForeColor="Blue"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblTime_Fromactual" runat="server" Text="Actual T.From" ForeColor="Pink" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Actual T.To" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtActualTimeTo" Text='<%# Bind("actualTimeTo") %>' runat="server"
                                            Width="100px" MaxLength="5" Font-Bold="False" ValidationGroup="vgOT"
                                            AutoPostBack="True" CssClass="general" Visible="false"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="txtActualTimeTo_MaskedEditExtender" runat="server"
                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                            CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True"
                                            Mask="99:99" MaskType="Time" TargetControlID="txtActualTimeTo"
                                            UserTimeFormat="TwentyFourHour"></cc1:MaskedEditExtender>
                                        <asp:Label ID="lblActualTimeTo" runat="server" Text='<%# Bind("actualTimeTo") %>' ForeColor="Blue"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblTime_Toactual" runat="server" Text="Actual T.To" ForeColor="Pink" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Act Drtn" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActualDuration" runat="server" Text='<%# Bind("actualDuration") %>' ForeColor="Red"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblDurationactual" runat="server" Text="Act Drtn" ForeColor="Pink" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>


                                 
                          

                                <asp:TemplateField HeaderText="IsProposed" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Button ID="btnIsProposed" runat="server" Enabled="true" CssClass="btnSubmitIcongambar" CommandName="liatPropose" />
                                        <asp:HiddenField ID="hfIsProposed" runat="server" Value='<%# Bind("IsProposed") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hf_purposeID" runat="server" Value='<%# Bind("ProposeId") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsAgree" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Button ID="btnIsAgree" runat="server" Enabled="false" CssClass="btnSubmitIcongambar" />
                                        <asp:HiddenField ID="hfIsOut" runat="server" Value='<%# Bind("IsOut") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfIsAgree" runat="server" Value='<%# Bind("isAgree") %>'></asp:HiddenField>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>

                               
                           

                            
                                <asp:TemplateField HeaderText="Agreement PIC" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblupdateby" runat="server" Text='<%# Bind("updateby") %>' CssClass="general"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                               
                                  <asp:TemplateField HeaderText="Agreement" Visible="true">
                                    <ItemTemplate>
                                        <asp:Button ID="btnAgree" runat="server" Width="104" Text ="Agree" CssClass ="btnSubmitIcon" CommandName="Agree" ToolTip="Agree to OT" />
                                       &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnDissAgree" runat="server" Width="104" Text="DissAgree" CssClass="btnRejectIcon" CommandName="DissAgree" ToolTip="DissAgree to OT" />
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>


                              



                            </Columns>
                            <RowStyle HorizontalAlign="Left" />



                        </asp:GridView>
                    </div>


                </asp:Panel>
                <div align="right" style="height: 20px">
                    &nbsp;&nbsp;&nbsp;
                         </div>


                <%--up03 lama--%>

                <%-- ------------------------------------------%>

                <table width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="30%"></td>
                </table>



                <asp:Panel ID="panelhidden" runat="server">
                    <asp:HiddenField ID="hf_empno" runat="server" />
                    <asp:HiddenField ID="hf_empname" runat="server" />
                    <asp:HiddenField ID="hf_loginTime" runat="server" />
                    <asp:HiddenField ID="hf_deptID" runat="server" />
                    <asp:HiddenField ID="hf_HuroDeptID" runat="server" />
                    <asp:HiddenField ID="hf_userlevel" runat="server" />

                    <asp:HiddenField ID="hf_taskID" runat="server" />
                    <asp:HiddenField ID="hf_taskName" runat="server" />
                    <asp:HiddenField ID="hf_menuID" runat="server" />
                    <asp:HiddenField ID="hf_userID" runat="server" />

                    <asp:HiddenField ID="hf_dept" runat="server" />
                    <asp:HiddenField ID="hf_divisi" runat="server" />

                    <asp:HiddenField ID="hf_divisiID" runat="server" />
                    <asp:HiddenField ID="hfTransfer" runat="server" />
                    <asp:HiddenField ID="hf_Add" runat="server" />
                    <asp:HiddenField ID="hf_Update" runat="server" />
                    <asp:HiddenField ID="hf_delete" runat="server" />
                    <asp:HiddenField ID="hf_ro" runat="server" />
                    <asp:HiddenField ID="hf_AllDataBool" runat="server" />
                    <asp:HiddenField ID="hf_PageNo" runat="server" />
                    <asp:HiddenField ID="HFOTDTID" runat="server" />
                    <asp:HiddenField ID="HFOTTypeForm" runat="server" />
                    <asp:HiddenField ID="hf_HuroDivisiID" runat="server" />
                    <asp:HiddenField ID="hf_modeView" runat="server" />
                    <asp:HiddenField ID="hfAppStep" runat="server" />
                    <asp:HiddenField ID="hfAppTaskStep" runat="server" />
                    <asp:HiddenField ID="hfAppTaskStepName" runat="server" />
                    <asp:HiddenField ID="hfFingerPrint" runat="server" />
                    <asp:HiddenField ID="hf_otDetails" runat="server" />
                    <asp:HiddenField ID="hf_isAgree" runat="server" />
                    <asp:HiddenField ID="hf_isPropose" runat="server" />
                    <asp:HiddenField ID="hf_ipaddress" runat="server" />
                    <asp:HiddenField ID="hf_proposerNo" runat="server" />
                    <asp:HiddenField ID="hf_test" runat="server" />
                    <asp:HiddenField ID="hf_type" runat="server" />
                    <asp:HiddenField ID="hf_deptemail" runat="server" />
                    <asp:HiddenField ID="hffriskainfo" runat="server" />
                    <asp:HiddenField ID="hf_tabOtApplyMain" runat="server" />

                    <asp:HiddenField ID="hf_onlineID" runat="server" />
                </asp:Panel>



            </ContentTemplate>
        </asp:UpdatePanel>

        <%----------------------%>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UPUtama"
            DisplayAfter="0" EnableViewState="true">
            <ProgressTemplate>
                <div id="Div1" align="center" valign="bottom" runat="server" style="position: fixed; top: 0; right: 0; bottom: 0; left: 0; visibility: visible; vertical-align: bottom; border-width: thin; border-color: transparent; border-bottom-style: solid; background-color: transparent; width: 100%; height: 200%; filter: alpha(opacity=20); width: 100%; height: 100%">
                    <div id="Div2" align="center" valign="bottom" runat="server" style="position: fixed; top: 1%; visibility: visible; background-color: transparent; width: 20%; left: 40%; top: 25%">
                        <img src="../../../Content/ProgressBar/indicator.white.gif" alt="Loading" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </form>
</body>
</html>
